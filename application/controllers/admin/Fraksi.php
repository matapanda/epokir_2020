<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Fraksi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Fraksi_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["fraksi"] = $this->Fraksi_model->getAll();
		$this->load->view("admin/fraksi/list", $data);
	}

	public function add()
	{
		$fraksi = $this->Fraksi_model;
		$validation = $this->form_validation;
		$validation->set_rules($fraksi->rules());

		if ($validation->run())
		{
			$fraksi->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
			redirect('admin/fraksi');
		}

		$this->load->view("admin/fraksi/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/fraksi');

		$fraksi = $this->Fraksi_model;
		$validation = $this->form_validation;
		$validation->set_rules($fraksi->rules());

		if ($validation->run())
		{
			$fraksi->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/fraksi');
		}

		$data["fraksi"] = $fraksi->getById($id);
		if (!$data["fraksi"]) show_404();

		$this->load->view("admin/fraksi/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Fraksi_model->delete($id))
		{
			redirect(site_url('admin/fraksi'));

		}
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Fraksi_model->select_all_fraksi();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "id");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "nama_fraksi");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama_fraksi);  
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data fraksi.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data fraksi.xlsx', NULL);
	}

}