<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Satuan_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['satuan'] = $this->Satuan_model->getAll();
		$this->load->view("admin/satuan/list", $data);
	}

	public function add()
	{
		$satuan = $this->Satuan_model;
		$validation = $this->form_validation;
		$validation->set_rules($satuan->rules());
		

		if ($validation->run())
		{
			$satuan->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/satuan');
		}

		$this->load->view("admin/satuan/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('satuan/volume');

		$satuan = $this->Satuan_model;
		$validation = $this->form_validation;
		$validation->set_rules($satuan->rules());

		if ($validation->run())
		{
			$satuan->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/satuan');
		}

		$data["satuan"] = $satuan->getById($id);
		if (!$data["satuan"]) show_404();

		$this->load->view("admin/satuan/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Satuan_model->delete($id))
		{
			redirect(site_url('admin/satuan'));
		}
	}

}

