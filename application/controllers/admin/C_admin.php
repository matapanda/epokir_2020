<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// session_start();
class C_admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
	}

	public function index()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/overview', $data); //??
	}

	public function logout()
	{
				$this->session->sess_destroy();
		redirect('auth');
	}

}

?>