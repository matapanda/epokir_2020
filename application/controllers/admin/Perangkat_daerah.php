<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Perangkat_daerah extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Perangkat_daerah_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["perangkat_daerah"] = $this->Perangkat_daerah_model->getAll();
		$this->load->view("admin/perangkat_daerah/list", $data);
	}

	public function add()
	{
		$perangkat_daerah = $this->Perangkat_daerah_model;
		$validation = $this->form_validation;
		$validation->set_rules($perangkat_daerah->rules());

		if ($validation->run())
		{
			$perangkat_daerah->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/perangkat_daerah');
		}

		$this->load->view("admin/perangkat_daerah/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/perangkat_daerah');

		$perangkat_daerah = $this->Perangkat_daerah_model;
		$validation = $this->form_validation;
		$validation->set_rules($perangkat_daerah->rules());

		if ($validation->run())
		{
			$perangkat_daerah->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/perangkat_daerah');
		}

		$data["perangkat_daerah"] = $perangkat_daerah->getById($id);
		if (!$data["perangkat_daerah"]) show_404();

		$this->load->view("admin/perangkat_daerah/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Perangkat_daerah_model->delete($id))
		{
			redirect(site_url('admin/perangkat_daerah'));
		}
	}


	public function export() {
		error_reporting(E_ALL);
    	require_once APPPATH.'third_party/PHPExcel.php';
		// include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Perangkat_daerah_model->select_all_perangkat_daerah();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Nama Perangkat");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Alamat");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Email");
	
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama_perangkat); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->alamat); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->email);
		     
		    // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Perangkat daerah.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Perangkat Daerah.xlsx', NULL);
	}

}