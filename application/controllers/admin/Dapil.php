<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dapil extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dapil_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["dapil"] = $this->Dapil_model->getAll();
		$this->load->view("admin/dapil/list", $data);
	}

	public function add()
	{
		$dapil = $this->Dapil_model;
		$validation = $this->form_validation;
		$validation->set_rules($dapil->rules());

		if ($validation->run())
		{
			$dapil->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/dapil');
		}

		$this->load->view("admin/dapil/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/dapil');

		$dapil = $this->Dapil_model;
		$validation = $this->form_validation;
		$validation->set_rules($dapil->rules());

		if ($validation->run())
		{
			$dapil->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/dapil');
		}

		$data["dapil"] = $dapil->getById($id);
		if (!$data["dapil"]) show_404();

		$this->load->view("admin/dapil/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Dapil_model->delete($id))
		{
			redirect(site_url('admin/dapil'));
		}
	}

	public function export() {
		error_reporting(E_ALL);
    	require_once APPPATH.'third_party/PHPExcel.php';
		// include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Dapil_model->select_all_dapil();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Nama");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Kode Pos");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->kode);  
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Dapil.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Dapil.xlsx', NULL);
	}

}