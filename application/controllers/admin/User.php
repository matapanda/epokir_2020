<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["user"] = $this->User_model->getAll();
		$this->load->view("admin/user/list", $data);
	}

	public function add()
	{
		$user = $this->User_model;
		$validation = $this->form_validation;
		$validation->set_rules($user->rules());

		$parser['dprd'] = $user->getAllDPRD();

		if ($validation->run())
		{
			$user->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/user/index');
		}

		$this->load->view("admin/user/new_form", $parser);
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/user');

		$user = $this->User_model;
		$validation = $this->form_validation;
		$validation->set_rules($user->rules());

		$data['dprd'] = $user->getAllDPRD();

		if ($validation->run())
		{
			$user->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
							redirect('admin/user');
		}

		$data["user"] = $user->getById($id);
		if (!$data["user"]) show_404();

		$this->load->view("admin/user/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->User_model->delete($id))
		{
			redirect(site_url('admin/user'));
		}
	}

	public function export() {
		error_reporting(E_ALL);
    	require_once APPPATH.'third_party/PHPExcel.php';
		// include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->User_model->select_all_user();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Nama");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Username");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Password");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Level");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->username); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->password); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->level); 
		    // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data User.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data User.xlsx', NULL);
	}

}