<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class aspirasi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Aspirasi_model');
		$this->load->helper(array('url'));
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["aspirasi"] = $this->Aspirasi_model->getAll();
		$this->load->view("admin/aspirasi/list", $data);
	}

	public function konfirmasi($id, $status) {
		$this->db->update('aspirasi', array('status' => $status), array('id' => $id));

		$this->session->set_flashdata('success', 'Berhasil Dikonfirmasi');
		redirect('admin/aspirasi');
	}

	public function tampil() {
		$data['aspirasi'] = $this->Aspirasi_model->select_all();
		$this->load->view('admin/aspirasi/list', $data);
	}

	public function add()
	{
		$aspirasi = $this->Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		$parser['dprd'] = $aspirasi->getAllDPRD();
		$parser['dapil'] = $aspirasi->getAllDapil();
		$parser['satuan'] = $aspirasi->getAllSatuan();
		$parser['perangkat_daerah'] = $aspirasi->getAllPerangkatDaerah();


		if ($validation->run())
		{
			$aspirasi->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/aspirasi/list');
		}

		$this->load->view("admin/aspirasi/new_form", $parser);
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/aspirasi');

		$aspirasi = $this->Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		$data['dprd'] = $aspirasi->getAllDPRD();
		$data['dapil'] = $aspirasi->getAllDapil();
		$data['satuan'] = $aspirasi->getAllSatuan();
		$data['perangkat_daerah'] = $aspirasi->getAllPerangkatDaerah();

		if ($validation->run())
		{
			$aspirasi->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/aspirasi/list');
		}

		$data["aspirasi"] = $aspirasi->getById($id);
		if (!$data["aspirasi"]) show_404();

		$this->load->view("admin/aspirasi/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Aspirasi_model->delete($id))
		{
			redirect(site_url('admin/aspirasi'));
		}
	}

	public function cetak(){
	    ob_start();
	    
	   	$data["aspirasi"] = $this->Aspirasi_model->getAll();
	   	// ob_start();

	    $this->load->view('preview', $data);
	    // $html = ob_get_contents();
	    //     	ob_end_clean();
	        
	    // require_once('./assets/html2pdf/html2pdf.class.php');
	    
	    // $pdf = new HTML2PDF('P','A4','en');
	    // $pdf->WriteHTML($html);
	    // $pdf->Output('Data Usulan.pdf', 'D');


	    // $content = ob_get_clean();

	    // $html2pdf = new Html2Pdf('L', 'A4', 'fr');
	    // $html2pdf->pdf->SetDisplayMode('fullpage');
	    // $html2pdf->writeHTML($content);
	    // $html2pdf->output('example05.pdf');
  }

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Aspirasi_model->getAll();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Usulan");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Volume");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "ID Satuan");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Lokasi");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Tanggal");
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "ID Perangkat daerah");
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "ID Anggota DPRD");
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "ID Dapil");
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Keterangan");
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->usulan); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->volume); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->id_satuan);
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->lokasi);
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->tanggal);
		    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $value->id_perangkat_daerah);
		    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $value->id_anggota_drpd);
		    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $value->id_dapil);  
		    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $value->keterangan); 
		    // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Aspirasi.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Aspirasi.xlsx', NULL);
	}

}