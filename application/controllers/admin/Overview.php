<?php

class Overview extends CI_Controller {
    public function __construct()
    {
	
		parent::__construct();
		$this->load->database();
		$this->load->model('Aspirasi_model');
		$this->load->model('Dprd_model');
		$this->load->helper(array('url'));
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["dprd"] = $this->Dprd_model->getAll();
        $data["aspirasi"] = $this->Aspirasi_model->getNew();
        $this->load->view("admin/overview", $data);
	}
}