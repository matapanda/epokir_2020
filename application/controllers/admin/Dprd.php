<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dprd extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Dprd_model');
		$this->load->helper(array('url'));
		$this->load->library('form_validation');

	}

	// create akan menampilkan form 
    public function create() 
    {
        // load model dan form helper 
        $this->load->model('Dprd_model');
        $this->load->helper('form_helper');
 
        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/dprd/new_form'),
            'dd_kelamin' => $this->Dprd_model->dd_kelamin(),
            'kelamin_selected' => $this->input->post('kelamin') ? $this->input->post('kelamin') : '$row->kelamin', 
            // untuk edit ganti '' menjadi data dari database misalnya $row->provinsi
	);
        
        $this->load->view('admin/dprd/new_form', $data);
    }

	public function index()
	{
		$data["dprd"] = $this->Dprd_model->getAll();
		$this->load->view("admin/dprd/list", $data);
	}
	

	public function add()
	{
		$dprd = $this->Dprd_model;
		$validation = $this->form_validation;
		$validation->set_rules($dprd->rules());

		$parser['dapil'] = $dprd->getAllDapil();
		$parser['fraksi'] = $dprd->getAllFraksi();

		if ($validation->run())
		{
			$dprd->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
			redirect('admin/dprd');
		}

		$this->load->view("admin/dprd/new_form", $parser);
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/dprd');

		$dprd = $this->Dprd_model;
		$validation = $this->form_validation;
		$validation->set_rules($dprd->rules());

		$data['dapil'] = $dprd->getAllDapil();
		$data['fraksi'] = $dprd->getAllFraksi();

		if ($validation->run())
		{
			if($dprd->update()){
				$this->session->set_flashdata('success', 'Berhasil Disimpan');
				redirect('admin/dprd');
			}

			
		}

		$data["dprd"] = $dprd->getById($id);
		if (!$data["dprd"]) show_404();

		$this->load->view("admin/dprd/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Dprd_model->delete($id))
		{
			redirect(site_url('admin/dprd'));
		}
	}

	public function export() {
		error_reporting(E_ALL);
    	require_once APPPATH.'third_party/PHPExcel.php';
		// include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Dprd_model->select_all_dprd();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Nama");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "ID Dapil");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "ID Fraksi");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Jenis Kelamin");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Nomor Telepon");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->nama); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->id_dapil);
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->id_fraksi); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->jenis_kelamin); 
		    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Anggota DPRD.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Anggota DPRD.xlsx', NULL);
	}

	// public function import() {
	// 	$this->form_validation->set_rules('excel', 'File', 'trim|required');



	// 	if ($_FILES['excel']['name'] == '') {
	// 		$this->session->set_flashdata('msg', 'File harus diisi');
	// 	} else {
	// 		$config['upload_path'] = './assets/excel/';
	// 		$config['allowed_types'] = 'xls|xlsx';
			
	// 		$this->load->library('upload', $config);
			
	// 		if ( ! $this->upload->do_upload('excel')){
	// 			$error = array('error' => $this->upload->display_errors());
	// 		}
	// 		else{
	// 			$data = $this->upload->data();
				
	// 			error_reporting(E_ALL);
	// 			date_default_timezone_set('Asia/Jakarta');

	// 			include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

	// 			$inputFileName = './assets/excel/' .$data['file_name'];
	// 			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
	// 			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

	// 			$index = 0;
	// 			foreach ($sheetData as $key => $value) {
	// 				if ($key != 1) {
	// 					$id = md5(DATE('ymdhms').rand());
	// 					$check = $this->Dprd_model->check_nama($value['B']);

	// 					if ($check != 1) {
	// 						$resultData[$index]['id'] = $id;
	// 						$resultData[$index]['nama'] = ucwords($value['B']);
	// 						$resultData[$index]['id_dapil'] = $value['C'];
	// 						$resultData[$index]['jenis_kelamin'] = $value['D'];
	// 						$resultData[$index]['notelp'] = $value['E'];
	// 					}
	// 				}
	// 				$index++;
	// 			}

	// 			unlink('./assets/excel/' .$data['file_name']);

	// 			if (count($resultData) != 0) {
	// 				$result = $this->Dprd_model->insert_batch($resultData);
	// 				if ($result > 0) {
	// 					$this->session->set_flashdata('msg', show_succ_msg('Data DPRD Berhasil diimport ke database'));
	// 					redirect('admin/dprd');
	// 				}
	// 			} else {
	// 				$this->session->set_flashdata('msg', show_msg('Data DPRD Gagal diimport ke database (Data Sudah terupdate)', 'warning', 'fa-warning'));
	// 				redirect('admin/dprd');
	// 			}

	// 		}
	//	}
	//}
}