<?php

class Overview extends CI_Controller {
    
    public function __construct()
    {
		parent::__construct();
		$this->load->database();
		$this->load->model("Dprd_model", "dm");
		$this->load->model('Aspirasi_model');
		$this->load->helper(array('url'));
	}

	public function index()
	{
        // load view dprd/overview.php
        $data['aspirasi']=$this->Aspirasi_model->getSlide();
        $this->load->view('masyarakat/index', $data);

	}

	public function about()
	{
        // load view dprd/overview.php
        
        // $this->load->view('masyarakat/about');

        $data['dprd']=$this->dm->getAll();
        $this->load->view('masyarakat/about', $data);
	}


		public function detail($id)
	{
	
	
		$data["dprd"] = $this->dm->getAllId($id);

        $this->load->view('masyarakat/detail_dpr', $data);
	}



	public function aspirasi()
	{
        // load view dprd/overview.php
        $data['aspirasi']=$this->Aspirasi_model->getDiterima(); //menampilkan verifikasi
        $this->load->view('masyarakat/blog', $data);
	}
	public function login()
	{
        // load view dprd/overview.php
        $this->load->view('masyarakat/login');
	}


}
