<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->model('Dapil_model');
	// 	$this->load->library('form_validation');

	// }

	public function index()
	{
		$this->load->view('login1');
	}

	public function cek_login()
	{
		// print_r($_SESSION);
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
				);

		$this->load->model('User_model'); //load model
		$hasil = $this->User_model->cek_user($data);

		if ($hasil->num_rows() == 1)
		{
			foreach ($hasil->result() as $sess)
			{
				// print_r($sess);
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['id'] = $sess->id;
				$sess_data['username'] = $sess->username;
				$sess_data['nama'] = $sess->nama;
				$sess_data['level'] = $sess->level;

				// print_r("<pre>");
				// $this->db->join();
				// $dprd = $this->db->get_where('anggota_dprd', array())->result();
				if($sess->id_dprd){
					$dprd = $this->db->get_where('anggota_dprd', array('id' => $sess->id_dprd))->result();
					// print_r($dprd);
					$sess_data['id_dprd'] = $dprd[0]->id;
					$sess_data['id_dapil'] = $dprd[0]->id_dapil;
					$sess_data['foto'] = $dprd[0]->foto;
	
				}
				
				$this->session->set_userdata($sess_data);
				

			}
			if ($this->session->userdata('level') == 'admin')
			{
				redirect('admin');
			}
			elseif ($this->session->userdata('level') == 'dprd') {
				redirect('dprd/c_dprd');
			}
		}
		else {
			echo "<script> alert('Gagal Login: Cek username, password:');history.go(-1);</script>";
		}

	}

}