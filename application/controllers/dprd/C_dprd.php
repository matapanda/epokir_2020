<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// session_start();
class C_dprd extends CI_Controller {

	public function __construct() {

					parent::__construct();
		$this->load->database();
		$this->load->model('C_Aspirasi_model');
			$this->load->model('Aspirasi_model');
				$this->load->model('Dprd_model');
		$this->load->library('form_validation');
		$this->load->helper(array('url'));
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
	}

	public function index()
	{
		$data["aspirasi"] = $this->C_Aspirasi_model->select_by_id();
		$data["aspirasi2"] = $this->Aspirasi_model->getAll();
		$data['username'] = $this->session->userdata('username');
		$this->load->view('dprd/overview', $data); //??
	}

	public function profile_dprd()
	{
		$data["profile_id"] =  $this->C_Aspirasi_model->profile_dprd_id();
		$this->load->view("dprd/profile", $data);
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('auth');
	}


}

?>