<?php

class Overview extends CI_Controller {
    public function __construct()
    {
			parent::__construct();
		$this->load->database();
		$this->load->model('C_Aspirasi_model');
		$this->load->library('form_validation');
		$this->load->helper(array('url'));

	}

	public function index()
	{
        // load view admin/overview.php
        $data["aspirasi"] = $this->C_Aspirasi_model->select_by_id();
        $this->load->view("drpds/overview", $data);
	}
}