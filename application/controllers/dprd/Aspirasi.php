<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class aspirasi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('C_Aspirasi_model');
		// $this->load->model('Aspirasi_model');
		$this->load->library('form_validation');
		$this->load->helper(array('url'));

	}

	public function index()
	{
		$data["aspirasi"] = $this->C_Aspirasi_model->select_by_id();
		$this->load->view("dprd/aspirasi/list", $data);
	}

	// public function tampil() {
	// 	$data['aspirasi'] = $this->C_Aspirasi_model->select_all();
	// 	$this->load->view('dprd/aspirasi/list', $data);
	// }

	public function add()
	{
		$aspirasi = $this->C_Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		$parser['dprd'] = $aspirasi->getAllDPRD();
		$parser['dapil'] = $aspirasi->getAllDapil();
		$parser['satuan'] = $aspirasi->getAllSatuan();
		$parser['perangkat_daerah'] = $aspirasi->getAllPerangkatDaerah();

		if ($validation->run())
		{	
			$date = hash("sha256", date("Y-m-d H:i:s"));

	        $path = './upload/aspirasi/';
	        if(!file_exists($path.'/')){
	        	mkdir($path.'/');
	        }

	        $config['upload_path']          = $path;
	        $config['allowed_types']        = "gif|jpg|png|JPG|JPEG|jpeg";
	        $config['max_size']             = 4024;
	        $config['overwrite']            = true;
	        $config['file_name']            = $date.".jpg";
	           
	        $upload_data = $this->do_upload($config, "foto_as");
	        
	        if($upload_data["status"]){
	            $data["file_loc"] = $upload_data["main_data"]["upload_data"]["file_name"];
	            // print_r($data["file_loc"]);
	            $simpan = $aspirasi->save($data["file_loc"]);
	            $this->session->set_flashdata('success', 'Berhasil Disimpan');
	            redirect('dprd/aspirasi');
				// if($simpan){
				// 	$this->session->set_flashdata('success', 'Berhasil Disimpan');
				// 	redirect('dprd/aspirasi');
				// }
	        }else{
	        	$this->session->set_flashdata('success', $upload_data["main_msg"]["error"]);
				redirect('dprd/aspirasi');
				print_r($upload_data["main_msg"]["error"]);
	        }
		}

		$this->load->view("dprd/aspirasi/new_form", $parser);
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('dprd/aspirasi');

		$aspirasi = $this->C_Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		$data['dprd'] = $aspirasi->getAllDPRD();
		$data['dapil'] = $aspirasi->getAllDapil();
		$data['satuan'] = $aspirasi->getAllSatuan();
		$data['perangkat_daerah'] = $aspirasi->getAllPerangkatDaerah();

		if ($validation->run())
		{
			$aspirasi->update();
			          $this->session->set_flashdata('success', 'Berhasil Disimpan');
            redirect('dprd/aspirasi/');
		}

		$data["aspirasi"] = $aspirasi->getById($id);
		if (!$data["aspirasi"]) show_404();

		$this->load->view("dprd/aspirasi/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->C_Aspirasi_model->delete($id))
		{
			redirect(site_url('dprd/aspirasi'));
		}
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->C_Aspirasi_model->getAll();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Usulan");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Volume");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "ID Satuan");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Lokasi");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Tanggal");
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "ID Perangkat daerah");
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "ID Anggota DPRD");
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "ID Dapil");
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Keterangan");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->usulan); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->volume); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->id_satuan);
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->lokasi);
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->tanggal);
		    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $value->id_perangkat_daerah);
		    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $value->id_anggota_drpd);
		    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $value->id_dapil);  
		    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $value->keterangan); 
		    // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Aspirasi.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Aspirasi.xlsx', NULL);
	}

	public function index_report(){
		$data["aspirasi"] = $this->C_Aspirasi_model->select_by_id();
		$this->load->view("dprd/aspirasi/report", $data);
	}

	public function get_arsip_report(){
		// print_r($_SESSION);
		$data["aspirasi"] = array();
		if(isset($_GET["tgl_start"]) && isset($_GET["tgl_finish"])){
			$id 		= $this->session->userdata('id_dprd');

			$tgl_start 	= $_GET["tgl_start"];
			$tgl_finish = $_GET["tgl_finish"];

			$where = array("id_anggota_dprd"=>$id);

			$this->db->select("ai.id AS id, ai.id_anggota_dprd AS id_anggota_dprd, ai.id_dapil AS id_dapil, ai.foto_as AS foto_as, dp.nama AS nama_dapil, ad.nama AS nama_dprd, ad.id AS id_dprd, ai.keterangan as keterangan, dp.id AS dapil_id, ai.tanggal AS tanggal, ai.usulan AS usulan, ai.lokasi AS lokasi, ai.status AS status, ai.id_satuan AS id_satuan, ai.volume as volume, st.id as satuan_id, st.nama_satuan as nama_satuan, ai.id_perangkat_daerah as id_perangkat_daerah, pd.nama_perangkat as nama_perangkat, pd.id as perangkat_daerah_id");

			$this->db->join("anggota_dprd ad", "ai.id_anggota_dprd = ad.id");
			$this->db->join("dapil dp", "ai.id_dapil = dp.id");
			$this->db->join("satuan st", " ai.id_satuan = st.id");
			$this->db->join("perangkat_daerah pd", "ai.id_perangkat_daerah = pd.id");

			$this->db->where('ai.tanggal >=', $tgl_start);
	        $this->db->where('ai.tanggal <=', $tgl_finish);
	        
	        $data["aspirasi"] = $this->db->get_where("aspirasi ai", $where)->result();
		}

		$this->load->view("dprd/aspirasi/report", $data);
	}

	public function print_arsip_report(){
		// print_r($_SESSION);
		$data["aspirasi"] = array();
		if(isset($_GET["tgl_start"]) && isset($_GET["tgl_finish"])){
			$id 		= $this->session->userdata('id_dprd');

			$tgl_start 	= $_GET["tgl_start"];
			$tgl_finish = $_GET["tgl_finish"];

			$where = array("id_anggota_dprd"=>$id);

			$this->db->select("ai.id AS id, ai.id_anggota_dprd AS id_anggota_dprd, ai.id_dapil AS id_dapil, ai.foto_as AS foto_as, dp.nama AS nama_dapil, ad.nama AS nama_dprd, ad.id AS id_dprd, ai.keterangan as keterangan, dp.id AS dapil_id, ai.tanggal AS tanggal, ai.usulan AS usulan, ai.lokasi AS lokasi, ai.status AS status, ai.id_satuan AS id_satuan, ai.volume as volume, st.id as satuan_id, st.nama_satuan as nama_satuan, ai.id_perangkat_daerah as id_perangkat_daerah, pd.nama_perangkat as nama_perangkat, pd.id as perangkat_daerah_id");

			$this->db->join("anggota_dprd ad", "ai.id_anggota_dprd = ad.id");
			$this->db->join("dapil dp", "ai.id_dapil = dp.id");
			$this->db->join("satuan st", " ai.id_satuan = st.id");
			$this->db->join("perangkat_daerah pd", "ai.id_perangkat_daerah = pd.id");

			$this->db->where('ai.tanggal >=', $tgl_start);
	        $this->db->where('ai.tanggal <=', $tgl_finish);
	        
	        $data["aspirasi"] = $this->db->get_where("aspirasi ai", $where)->result();
		}

		// $this->load->view("dprd/aspirasi/report", $data);

		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$suip = $data["aspirasi"];

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Usulan");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Volume");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "ID Satuan");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Lokasi");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Tanggal");
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "ID Perangkat daerah");
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "ID Anggota DPRD");
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "ID Dapil");
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "Keterangan");
		//$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Foto");
		$rowCount++;

		foreach($suip as $value){
			print_r($value);
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->id); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->usulan); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->volume); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->id_satuan);
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->lokasi);
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->tanggal);
		    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $value->id_perangkat_daerah);
		    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $value->id_anggota_dprd);
		    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $value->id_dapil);  
		    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $value->keterangan); 
		    // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $value->notelp, PHPExcel_Cell_DataType::TYPE_STRING); 
		    //$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->foto); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Aspirasi.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Aspirasi.xlsx', NULL);
	}

	function cetak(){
		ob_start();
	    $data["aspirasi"] = array();
		if(isset($_GET["tgl_start"]) && isset($_GET["tgl_finish"])){
			$id 		= $this->session->userdata('id_dprd');

			$tgl_start 	= $_GET["tgl_start"];
			$tgl_finish = $_GET["tgl_finish"];

			$where = array("id_anggota_dprd"=>$id);

			$this->db->select("ai.id AS id, ai.id_anggota_dprd AS id_anggota_dprd, ai.id_dapil AS id_dapil, ai.foto_as AS foto_as, dp.nama AS nama_dapil, ad.nama AS nama_dprd, ad.id AS id_dprd, ai.keterangan as keterangan, dp.id AS dapil_id, ai.tanggal AS tanggal, ai.usulan AS usulan, ai.lokasi AS lokasi, ai.status AS status, ai.id_satuan AS id_satuan, ai.volume as volume, st.id as satuan_id, st.nama_satuan as nama_satuan, ai.id_perangkat_daerah as id_perangkat_daerah, pd.nama_perangkat as nama_perangkat, pd.id as perangkat_daerah_id");

			$this->db->join("anggota_dprd ad", "ai.id_anggota_dprd = ad.id");
			$this->db->join("dapil dp", "ai.id_dapil = dp.id");
			$this->db->join("satuan st", " ai.id_satuan = st.id");
			$this->db->join("perangkat_daerah pd", "ai.id_perangkat_daerah = pd.id");

			$this->db->where('ai.tanggal >=', $tgl_start);
	        $this->db->where('ai.tanggal <=', $tgl_finish);
	        
	        $data["aspirasi"] = $this->db->get_where("aspirasi ai", $where)->result();
	        ob_start();
		    $this->load->view('preview_ex', $data);
		    $html = ob_get_contents();
		        ob_end_clean();

		    // print_r($data["aspirasi"]);
		        
		        require_once('./assets/html2pdf/html2pdf.class.php');
		    $pdf = new HTML2PDF('L','A3','en');
		    $pdf->WriteHTML($html);
		    $pdf->Output('Data Usulan.pdf', 'D');
		}
	}

	// public function cetak(){
	//     ob_start();
	    
	//    $data["aspirasi"] = $this->Aspirasi_model->getAll();
	//    print_r($data["aspirasi"]);
	//    ob_start();
	//     $this->load->view('preview', $data);
	//     $html = ob_get_contents();
	//         ob_end_clean();
	        
	//         require_once('./assets/html2pdf/html2pdf.class.php');
	//     $pdf = new HTML2PDF('L','A3','en');
	//     $pdf->WriteHTML($html);
	//     $pdf->Output('Data Usulan.pdf', 'D');
	//  }

	function set_upload(){
		if(isset($_FILES["file_upload"])){
    		$periode = "2019";
	        $path = './upload/aspirasi/';
	        if(!file_exists($path.$periode.'/')){
	        	mkdir($path.$periode.'/');
	        }
	        $path .= $periode.'/';

        	$insert = $this->db->query("select insert_upload('$keterangan') as id")->row_array();
        	if($insert){
        		$config['upload_path']          = $path;
		        $config['allowed_types']        = "xlsx";
		        $config['max_size']             = 4096;
		        $config['file_name']            = hash("sha256", $insert["id"]).".xlsx";
		           
		        $upload_data = $this->uploadfilev0->do_upload($config, "file_upload");
		        
		        if($upload_data["status"]){
		            $where = array("id"=>$insert["id"]);
		            $set = array("file_loc"=>$upload_data["main_data"]["upload_data"]["file_name"]);

		            if($this->mm->update_data("upload_v0", $set, $where)){
		                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
		            }else {
		                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
		            }
		        }else{
		            $msg_detail["file"] = $upload_data["main_msg"]["error"];
		            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
		        }
        	}
        	// print_r($insert);
    	}else{
    		$msg_detail["file"] = $this->response_message->get_error_msg("UPLOAD_FAIL");
    	}
	}

	public function do_upload($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }

    function boy(){
    	print_r($_SESSION);
    }

}