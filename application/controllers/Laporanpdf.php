<?php
Class Laporanpdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    function index(){
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUSAN NEEGRI 2 LANGSA',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'USULAN ANGGOTA DPRD KOTA MALANG',0,1,'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(20,8,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,6,'NO',1,0);
        $pdf->Cell(65,8,'USULAN',1,0);
        $pdf->Cell(17,6,'VOLUME',1,0);
        $pdf->Cell(25,6,'LOKASI',1,0);
        $pdf->Cell(26,7,'TANGGAL USULAN',1,1);
       /* $pdf->Cell(28,6,'PERANGGKAT DAERAH',1,0);
        $pdf->Cell(25,6,'ANGGOTA DPRD',1,0);
        $pdf->Cell(25,6,'DAPIL',1,0);
        $pdf->Cell(25,6,'KET',1,0);
*/
        $pdf->SetFont('Arial','',10);

        $mahasiswa = $this->db->get('aspirasi')->result();

        foreach ($mahasiswa as $row){
            $pdf->Cell(10,6,$row->id,1,0);
            $pdf->Cell(65,6,$row->usulan,1,0);
            $pdf->Cell(17,6,$row->volume,1,0);
            $pdf->Cell(25,6,$row->lokasi,1,0);
            $pdf->Cell(26,7,$row->tanggal,1,1); 
          /*  $pdf->Cell(25,6,$row->id_perangkat_daerah,1,1); 
            $pdf->Cell(25,6,$row->id_anggota_dprd,1,1);
            $pdf->Cell(25,6,$row->id_dapil,1,1);
            $pdf->Cell(25,6,$row->keterangan,1,1);    */
        }

        $pdf->Output();
    }
}