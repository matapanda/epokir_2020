<?php
class Aspirasi_model extends CI_Model {
 
    private $_table = "aspirasi";

    public $id;
    public $usulan;
    public $volume;
    public $id_satuan;
    public $lokasi;
    public $tanggal;
    public $id_perangkat_daerah;
    public $id_anggota_dprd;
    public $id_dapil;
    public $keterangan;
    public $status;

    public function select_all_aspirasi() {
        $sql = "SELECT * FROM aspirasi";

        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getAllDapil() {
        return $this->db->get('dapil')->result_array();
    }

    public function getAllDPRD() {
        return $this->db->get('anggota_dprd')->result_array();
    }

    public function getAllSatuan()
    {
        return $this->db->get('satuan')->result_array();
    }

    public function getAllPerangkatDaerah()
    {
        return $this->db->get('perangkat_daerah')->result_array();
    }

    public function rules()
    {
        return [

            ['field' => 'tanggal',
            'label' => 'tanggal',
            'rules' => 'required'],
            
            ['field' => 'usulan',
            'label' => 'usulan',
            'rules' => 'required'],

            ['field' => 'lokasi',
            'label' => 'lokasi',
            'rules' => 'required']

        ];
    }

    // public function getAll()
    // {
    // 	return $this->db->get($this->_table)->result();
    // }

    public function getAll()
    {
        // return $this->db->get($this->_table)->result();
        $sql = "SELECT aspirasi.id AS id, 
                aspirasi.id_anggota_dprd AS id_anggota_dprd, 
                aspirasi.id_dapil AS id_dapil, 
                dapil.nama AS nama_dapil, 
                anggota_dprd.nama AS nama_dprd, 
                anggota_dprd.id AS id_dprd,
                aspirasi.keterangan as keterangan, 
                dapil.id AS dapil_id, 
                aspirasi.tanggal AS tanggal, 
                aspirasi.usulan AS usulan, 
                aspirasi.lokasi AS lokasi, aspirasi.status AS status, 
                aspirasi.id_satuan AS id_satuan, aspirasi.volume as volume, 
                satuan.id as satuan_id, satuan.nama_satuan as nama_satuan, 
                aspirasi.id_perangkat_daerah as id_perangkat_daerah, 
                perangkat_daerah.nama_perangkat as nama_perangkat, 
                perangkat_daerah.id as perangkat_daerah_id, 
                aspirasi.foto_as AS foto_aspirasi 
                FROM aspirasi, anggota_dprd, dapil, satuan, perangkat_daerah 
                WHERE aspirasi.id_anggota_dprd = anggota_dprd.id AND aspirasi.id_dapil = dapil.id 
                AND aspirasi.id_satuan = satuan.id AND aspirasi.id_perangkat_daerah = perangkat_daerah.id 
                ORDER BY aspirasi.tanggal DESC";
        
        $data = $this->db->query($sql);
        return $data->result();
    }

    //nampilno sing di verif tok
    public function getDiterima()
    {
        // return $this->db->get($this->_table)->result();
        $sql = " SELECT aspirasi.id AS id, aspirasi.id_anggota_dprd AS id_anggota_dprd, aspirasi.id_dapil AS id_dapil, dapil.nama AS nama_dapil, anggota_dprd.nama AS nama_dprd, anggota_dprd.id AS id_dprd, aspirasi.keterangan as keterangan, dapil.id AS dapil_id, aspirasi.tanggal AS tanggal, aspirasi.usulan AS usulan, aspirasi.lokasi AS lokasi, aspirasi.status AS status, aspirasi.id_satuan AS id_satuan, aspirasi.volume as volume, satuan.id as satuan_id, satuan.nama_satuan as nama_satuan, aspirasi.id_perangkat_daerah as id_perangkat_daerah, perangkat_daerah.nama_perangkat as nama_perangkat, perangkat_daerah.id as perangkat_daerah_id FROM aspirasi, anggota_dprd, dapil, satuan, perangkat_daerah WHERE aspirasi.id_anggota_dprd = anggota_dprd.id AND aspirasi.id_dapil = dapil.id AND aspirasi.id_satuan = satuan.id AND aspirasi.id_perangkat_daerah = perangkat_daerah.id AND aspirasi.status = 'Diterima' ORDER BY aspirasi.tanggal DESC; ";

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function getSlide()
    {
        // return $this->db->get($this->_table)->result();
        $sql = " SELECT aspirasi.id AS id, aspirasi.id_anggota_dprd AS id_anggota_dprd, aspirasi.id_dapil AS id_dapil, dapil.nama AS nama_dapil, anggota_dprd.nama AS nama_dprd, anggota_dprd.id AS id_dprd, aspirasi.keterangan as keterangan, dapil.id AS dapil_id, aspirasi.tanggal AS tanggal, aspirasi.usulan AS usulan, aspirasi.lokasi AS lokasi, aspirasi.status AS status, aspirasi.id_satuan AS id_satuan, aspirasi.volume as volume, satuan.id as satuan_id, satuan.nama_satuan as nama_satuan, aspirasi.id_perangkat_daerah as id_perangkat_daerah, perangkat_daerah.nama_perangkat as nama_perangkat, perangkat_daerah.id as perangkat_daerah_id FROM aspirasi, anggota_dprd, dapil, satuan, perangkat_daerah WHERE aspirasi.id_anggota_dprd = anggota_dprd.id AND aspirasi.id_dapil = dapil.id AND aspirasi.id_satuan = satuan.id AND aspirasi.id_perangkat_daerah = perangkat_daerah.id AND aspirasi.status = 'Diterima' ORDER BY aspirasi.tanggal DESC LIMIT 3; ";

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function insert_batch($data) {
        $this->db->insert_batch('perangkat_daerah', $data);
        
        return $this->db->affected_rows();
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	$this->id = "";
    	$this->usulan = $post["usulan"];
        $this->volume = $post["volume"];
        $this->id_satuan = $post["id_satuan"];
        $this->lokasi = $post["lokasi"];
        $this->tanggal = date('Y-m-d', strtotime($post["tanggal"]));
        $this->id_perangkat_daerah = $post["id_perangkat_daerah"];
        $this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->keterangan = $post["keterangan"];
        
        
    	$this->db->insert($this->_table, $this);

    }

    
    public function update()
    {
    	$post = $this->input->post();
        $this->usulan = $post["usulan"];
        $this->volume = $post["volume"];
        $this->id_satuan = $post["id_satuan"];
        $this->lokasi = $post["lokasi"];
        $this->tanggal = date('Y-m-d', strtotime($post["tanggal"]));
        $this->id_perangkat_daerah = $post["id_perangkat_daerah"];
        $this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->keterangan = $post["keterangan"];
        
    	
    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }


    public function delete($id)
    {
        $this->_deleteImage($id);
    	return $this->db->delete($this->_table, array('id' => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $dprd = $this->getById($id);
        if ($dprd->foto != "default.jpg") {
            $filename = explode(".", $dprd->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
        }
    }

     public function getNew()
    {
        // return $this->db->get($this->_table)->result();
        $sql = "SELECT aspirasi.id AS id, aspirasi.id_anggota_dprd AS id_anggota_dprd, aspirasi.id_dapil AS id_dapil, dapil.nama AS nama_dapil, anggota_dprd.nama AS nama_dprd, anggota_dprd.id AS id_dprd, aspirasi.keterangan as keterangan, dapil.id AS dapil_id, aspirasi.tanggal AS tanggal, aspirasi.usulan AS usulan, aspirasi.lokasi AS lokasi, aspirasi.status AS status, aspirasi.id_satuan AS id_satuan, aspirasi.volume as volume, satuan.id as satuan_id, satuan.nama_satuan as nama_satuan, aspirasi.id_perangkat_daerah as id_perangkat_daerah, perangkat_daerah.nama_perangkat as nama_perangkat, perangkat_daerah.id as perangkat_daerah_id FROM aspirasi, anggota_dprd, dapil, satuan, perangkat_daerah WHERE aspirasi.id_anggota_dprd = anggota_dprd.id AND aspirasi.id_dapil = dapil.id AND aspirasi.id_satuan = satuan.id AND aspirasi.id_perangkat_daerah = perangkat_daerah.id ORDER BY aspirasi.tanggal DESC LIMIT 5";

        $data = $this->db->query($sql);
        return $data->result();
    }

}