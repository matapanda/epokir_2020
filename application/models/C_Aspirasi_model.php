<?php
class C_Aspirasi_model extends CI_Model {
 
    private $_table = "aspirasi";

    public $id;
    public $usulan;
    public $volume;
    public $id_satuan;
    public $lokasi;
    public $tanggal;
    public $id_perangkat_daerah;
    public $id_anggota_dprd;
    public $id_dapil;
    public $foto_as;
    public $keterangan;
    public $status;

    // public function select_all_aspirasi() {
    //     $sql = "SELECT * FROM aspirasi";

    //     $data = $this->db->query($sql);

    //     return $data->result();
    // }

    public function getAllDapil() {
        return $this->db->get('dapil')->result_array();
    }

    public function getAllDPRD() {
        return $this->db->get('anggota_dprd')->result_array();
    }

    public function getAllSatuan()
    {
        return $this->db->get('satuan')->result_array();
    }

    public function getAllPerangkatDaerah()
    {
        return $this->db->get('perangkat_daerah')->result_array();
    }

    public function rules()
    {
        return [

            ['field' => 'tanggal',
            'label' => 'tanggal',
            'rules' => 'required'],
            
            ['field' => 'usulan',
            'label' => 'usulan',
            'rules' => 'required'],

            ['field' => 'lokasi',
            'label' => 'lokasi',
            'rules' => 'required']

        ];
    }

    // public function getAll()
    // {
    // 	return $this->db->get($this->_table)->result();
    // }

    public function getAll()
    {
        // return $this->db->get($this->_table)->result();
        $sql = "SELECT u.nama as nama_dprd_id, ad.jenis_kelamin, ad.notelp, ad.foto as foto_dprd, d.nama as nama_dapil, d.kode, f.nama_fraksi FROM user u JOIN anggota_dprd ad ON u.id_dprd = ad.id JOIN dapil d ON d.id = ad.id_dapil JOIN fraksi f ON f.id = ad.id_fraksi";

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function profile_dprd_id()
    {
        $id = $this->session->userdata('id');
        $sql = "SELECT u.nama as nama_dprd_id, ad.jenis_kelamin, ad.notelp, ad.foto as foto_dprd, d.nama as nama_dapil, d.kode, f.nama_fraksi FROM user u JOIN anggota_dprd ad ON u.id_dprd = ad.id JOIN dapil d ON d.id = ad.id_dapil JOIN fraksi f ON f.id = ad.id_fraksi WHERE u.id = '$id'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function profile_dprd_web(){
         $sql = "SELECT u.nama as nama_dprd_id, ad.jenis_kelamin, ad.notelp, ad.foto as foto_dprd, d.nama as nama_dapil, d.kode, f.nama_fraksi FROM user u JOIN anggota_dprd ad ON u.id_dprd = ad.id JOIN dapil d ON d.id = ad.id_dapil JOIN fraksi f ON f.id = ad.id_fraksi";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function select_by_id()
    {
        //gae nyeluk session
        $id = $this->session->userdata('id_dprd');

        $sql = " SELECT aspirasi.id AS id, aspirasi.id_anggota_dprd AS id_anggota_dprd, aspirasi.foto_as AS foto_as, aspirasi.id_dapil AS id_dapil, dapil.nama AS nama_dapil, anggota_dprd.nama AS nama_dprd, anggota_dprd.id AS id_dprd, aspirasi.keterangan as keterangan, dapil.id AS dapil_id, aspirasi.tanggal AS tanggal, aspirasi.usulan AS usulan, aspirasi.lokasi AS lokasi, aspirasi.status AS status, aspirasi.id_satuan AS id_satuan, aspirasi.volume as volume, satuan.id as satuan_id, satuan.nama_satuan as nama_satuan, aspirasi.id_perangkat_daerah as id_perangkat_daerah, perangkat_daerah.nama_perangkat as nama_perangkat, perangkat_daerah.id as perangkat_daerah_id FROM aspirasi, anggota_dprd, dapil, satuan, perangkat_daerah WHERE aspirasi.id_anggota_dprd = anggota_dprd.id AND aspirasi.id_dapil = dapil.id AND aspirasi.id_satuan = satuan.id AND aspirasi.id_perangkat_daerah = perangkat_daerah.id AND aspirasi.id_anggota_dprd = $id; ";

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function insert_batch($data) {
        $this->db->insert_batch('perangkat_daerah', $data);
        
        return $this->db->affected_rows();
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save($foto_name)
    {
    	$post = $this->input->post();
    	$this->id = "";
        $this->usulan = $post["usulan"];
        $this->volume = $post["volume"];
        $this->id_satuan = $post["id_satuan"];
        $this->lokasi = $post["lokasi"];
        $this->tanggal = date('Y-m-d', strtotime($post["tanggal"]));
        $this->id_perangkat_daerah = $post["id_perangkat_daerah"];
        $this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->foto_as = $foto_name;
        $this->keterangan = $post["keterangan"];
        $this->status = 'Belum Dikonfirmasi';

        $this->db->insert($this->_table, $this);

    }

    public function update()
    {
    	$post = $this->input->post();
        $this->id = $post["id"];
        $this->usulan = $post["usulan"];
        $this->volume = $post["volume"];
        $this->id_satuan = $post["id_satuan"];
        $this->lokasi = $post["lokasi"];
        $this->tanggal = $post["tanggal"];
        $this->id_perangkat_daerah = $post["id_perangkat_daerah"];
        $this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->keterangan = $post["keterangan"];
        $this->status = 'Belum Dikonfirmasi';
        
          if (!empty($_FILES["foto_as"]["name"])) {
            $config['upload_path']          = './upload/aspirasi/';
            $config['allowed_types']        = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size']             = 2048;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('foto_as'))
            {
                $this->foto_as = $this->upload->display_errors();
            }
            else
            {
                $image = $this->upload->data();

                $this->foto_as = $image['file_name'];
            }
        } else {
            $this->foto_as = $post["old_image"];
        }


        $this->db->update($this->_table, $this, array('id' => $post['id']));
    }


    public function delete($id)
    {
        // $this->_deleteImage($id);
    	return $this->db->delete($this->_table, array('id' => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $dprd = $this->getById($id);
        if ($dprd->foto != "default.jpg") {
            $filename = explode(".", $dprd->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
        }
    }
}