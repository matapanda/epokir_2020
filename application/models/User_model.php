<?php
class User_model extends CI_Model {

	private $_table = "user";

	public $id;
    public $nama;
    public $username;
    public $password;
    public $foto;
    public $level;
    public $id_dprd;

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	parent::set_table('user', 'id');
	// }


    public function getAllDPRD() {
        return $this->db->get('anggota_dprd')->result_array();
    }

    //  public function insert_batch($data) {
    //     $this->db->insert_batch('dprd', $data);
        
    //     return $this->db->affected_rows();
    // }

	public function rules()
    {
        return [
            // ['field' => 'nama',
            // 'label' => 'Nama',
            // 'rules' => 'required'],
            
            ['field' => 'username',
            'label' => 'Username',
            'rules' => 'required'],

            ['field' => 'password',
            'label' => 'Password',
            'rules' => 'required'],

            ['field' => 'level',
            'label' => 'Level',
            'rules' => 'required']
        ];
    }

	public function get_by_username($username)
	{
		if ($query = parent::get_by(array('username' => $username)))
		{
			if ($query->id != NULL) 
				return $query;
		}
		return false;
	}

	public function cek_user($data)
	{	
		$query = $this->db->get_where('user', $data);
		return $query;
	}

    public function select_all_user() {
        $sql = "SELECT * FROM user";

        $data = $this->db->query($sql);

        return $data->result();
    }

	//baruu
	public function getAll()
    {
    	// return $this->db->get($this->_table)->result();

        $sql = " SELECT user.id AS id, user.nama AS nama, user.username AS username, user.password AS password, user.foto AS foto, user.level AS level, user.id_dprd AS id_dprd, anggota_dprd.id AS id_anggota_dprd, anggota_dprd.nama AS nama_anggota_dprd FROM user, anggota_dprd WHERE user.id_dprd = anggota_dprd.id ";

        $data = $this->db->query($sql);
        return $data->result();

    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

	public function save()
    {
    	$post = $this->input->post();
    	//$this->id = uniqid();
        $id = $post['id_dprd'];
        $dprd = "SELECT * FROM anggota_dprd WHERE id = $id";
        $data = $this->db->query($dprd)->result_array();

    	$this->nama = $data[0]["nama"];
    	$this->username = $post["username"];
    	$this->password = md5($post["password"]);
    	// $this->foto = $post["foto"];
    	$this->level = $post["level"];
        $this->id_dprd = $post["id_dprd"];
    	$this->db->insert($this->_table, $this);
    }

    public function update()
    {
    	$post = $this->input->post();

        $id = $post['id_dprd'];
        $dprd = "SELECT * FROM anggota_dprd WHERE id = $id";
        $data = $this->db->query($dprd)->result_array();

    	$this->id = $post["id"];

    	$this->nama = $data[0]["nama"];
    	$this->username = $post["username"];
    	$this->password = md5($post["password"]);
    	// $this->foto = $post["foto"];
    	$this->level = $post["level"];
        $this->id_dprd = $post["id_dprd"];
    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
    	return $this->db->delete($this->_table, array('id' => $id));
    }


}


?>