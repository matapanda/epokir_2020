<?php
class Dprd_model extends CI_Model {
 
    private $_table = "anggota_dprd";
    // private $_table = "Kelamin";

    public $id;
    public $nama;
    public $id_dapil;
    public $id_fraksi;
    public $jenis_kelamin;
    public $notelp;
    public $foto;

    public function getAllDapil() {
        return $this->db->get('dapil')->result_array();
    }

    public function getAllFraksi() {
        return $this->db->get('fraksi')->result_array();
    }

    public function insert_batch($data) {
        $this->db->insert_batch('dprd', $data);
        
        return $this->db->affected_rows();
    }

    public function rules()
    {
        return [

            ['field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'],
            
            ['field' => 'jenis_kelamin',
            'label' => 'Kelamin',
            'rules' => 'required'],

            ['field' => 'notelp',
            'label' => 'No Telepon',
            'rules' => 'numeric']
        ];
    }

    // function dd_kelamin()
    // {
    //     // ambil data dari db
    //     $this->db->order_by('id_kelamin', 'asc');
    //     $result = $this->db->get('kelamin');
        
    //     // bikin array
    //     // please select berikut ini merupakan tambahan saja agar saat pertama
    //     // diload akan ditampilkan text please select.
    //     $dd[''] = 'Please Select';
    //     if ($result->num_rows() > 0) {
    //         foreach ($result->result() as $row) {
    //         // tentukan value (sebelah kiri) dan labelnya (sebelah kanan)
    //             $dd[$row->id_kelamin] = $row->nama;
    //         }
    //     }
    //     return $dd;
    // }

    public function select_all_dprd() {
        $sql = "SELECT * FROM anggota_dprd";

        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getAll()
    {
    	// return $this->db->get($this->_table)->result();
        $sql = " SELECT anggota_dprd.id AS id, anggota_dprd.nama AS nama, 
        dapil.id AS id_dapil, dapil.nama AS nama_dapil, anggota_dprd.id_fraksi AS id_fraksi, 
        fraksi.id AS fraksi_id, fraksi.nama_fraksi AS nama_fraksi, 
        anggota_dprd.jenis_kelamin AS jenis_kelamin, anggota_dprd.notelp AS notelp, anggota_dprd.foto AS foto FROM anggota_dprd, dapil, fraksi WHERE anggota_dprd.id_dapil = dapil.id AND anggota_dprd.id_fraksi = fraksi.id ORDER BY anggota_dprd.id ASC";

        $data = $this->db->query($sql);
        return $data->result();
    }
    // hahaha

    public function getAllid($id)
    {
         $sql =$this->db->query("SELECT u.nama as nama_dprd_id, ad.jenis_kelamin, ad.notelp, ad.foto as foto_dprd, d.nama as nama_dapil, d.kode, f.nama_fraksi FROM user u JOIN anggota_dprd ad ON u.id_dprd = ad.id JOIN dapil d ON d.id = ad.id_dapil JOIN fraksi f ON f.id = ad.id_fraksi WHERE ad.id='".$id."'");

    
        return $sql->result();  
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	//$this->id = uniqid();
    	$this->nama = $post["nama"];
        $this->id_dapil = $post["id_dapil"];
        $this->id_fraksi = $post["id_fraksi"];
        $this->jenis_kelamin = $post["jenis_kelamin"];
        $this->notelp = $post["notelp"];

        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png|JPEG|JPG|jpeg';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('foto'))
        {
            $this->foto = $this->upload->display_errors();
        }
        else
        {
            $image = $this->upload->data();

            $this->foto = $image['file_name'];
        }
            
    	$this->db->insert($this->_table, $this);

    }


    public function update()
    {
    	$post = $this->input->post();
    	$this->id = $post["id"];
    	$this->nama = $post["nama"];
    	$this->id_dapil = $post["id_dapil"];
        $this->id_fraksi = $post["id_fraksi"];
        $this->jenis_kelamin = $post["jenis_kelamin"];
        $this->notelp = $post["notelp"];
        
        if (!empty($_FILES["foto"]["name"])) {
            $config['upload_path']          = './upload/product/';
            $config['allowed_types']        = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size']             = 2048;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('foto'))
            {
                $this->foto = $this->upload->display_errors();
            }
            else
            {
                $image = $this->upload->data();

                $this->foto = $image['file_name'];
            }
        } else {
            $this->foto = $post["old_image"];
        }

    	$update = $this->db->update($this->_table, $this, array('id' => $post['id']));
        return $update;
    }


    public function delete($id)
    {
        $this->_deleteImage($id);
    	return $this->db->delete($this->_table, array('id' => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|JPG|JPEG';
        $config['file_name']            = 'test';
        $config['overwrite']            = true;
        $config['max_size']             = 4024; // 4MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        // if ($this->upload->do_upload('foto')) {
        //     return $this->upload->data("file_name");
        // } else {
        //     return "default.jpg";
        // }

        if ( ! $this->upload->do_upload('foto'))
        {
            return "dsa.jpg";
        }
        else
        {
            $data = $this->upload->data();

            return $data['file_name'];
        }
    }

    private function _deleteImage($id)
    {
        $dprd = $this->getById($id);
        if ($dprd->foto != "default.jpg") {
            $filename = explode(".", $dprd->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
    }

}
}