<?php
class Dapil_model extends CI_Model {
 
    private $_table = "dapil";

    public $id;
    public $nama;
    public $kode;

    public function rules()
    {
        return [
            ['field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'],
            
            ['field' => 'kode',
            'label' => 'Kode',
            'rules' => 'required']
        ];
    }

    public function select_all_dapil() {
        $sql = "SELECT * FROM dapil";

        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getAll()
    {
    	return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	// $this->id = uniqid();
    	$this->nama = $post["nama"];
    	$this->kode = $post["kode"];
    	$this->db->insert($this->_table, $this);
    }

    public function update()
    {
    	$post = $this->input->post();
    	$this->id = $post["id"];
    	$this->nama = $post["nama"];
    	$this->kode = $post["kode"];
    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
    	return $this->db->delete($this->_table, array('id' => $id));
    }


}