<?php
class Perangkat_daerah_model extends CI_Model {
 
    private $_table = "perangkat_daerah";

    public $id;
    public $nama_perangkat;
    public $alamat;
    public $email;

    public function rules()
    {
        return [
            ['field' => 'nama_perangkat',
            'label' => 'Nama_Perangkat',
            'rules' => 'required'],
            
            ['field' => 'alamat',
            'label' => 'Alamat',
            'rules' => 'required'],

            ['field' => 'email',
            'label' => 'email',
            'rules' => 'required']

        ];
    }

    public function select_all_perangkat_daerah() {
        $sql = "SELECT * FROM perangkat_daerah";

        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getAll()
    {
    	return $this->db->get($this->_table)->result();

        // $sql = "SELECT * FROM perangkat_daerah ORDER BY nama_perangkat ASC";

        // $data = $this->db->query($sql);
        // return $data->result(); 
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	// $this->id = uniqid();
    	$this->nama_perangkat = $post["nama_perangkat"];
    	$this->alamat = $post["alamat"];
        $this->email = $post["email"];
    	$this->db->insert($this->_table, $this);
    }

    public function update()
    {
    	$post = $this->input->post();
    	$this->id = $post["id"];
    	$this->nama_perangkat = $post["nama_perangkat"];
    	$this->alamat = $post["alamat"];
        $this->email = $post["email"];
    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
    	return $this->db->delete($this->_table, array('id' => $id));
    }


}