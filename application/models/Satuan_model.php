<?php
class Satuan_model extends CI_Model {
 
    private $_table = "satuan";

    public $id;
    public $nama_satuan;

    public function rules()
    {
        return [
            ['field' => 'nama_satuan',
            'label' => 'Nama_Satuan',
            'rules' => 'required']
        ];
    }

    public function select_all_volume() {
        
        $sql = "SELECT * FROM satuan";

        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        // $this->id = uniqid();
        $this->nama_satuan = $post["nama_satuan"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->nama_satuan = $post["nama_satuan"];
        $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }


}