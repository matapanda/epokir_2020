<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>e-POKIR</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

</head>
<body>
<!-- <?php echo form_open("Auth/cek_login"); ?>

		<p>Username : <br>
		<input type="text" name="username">
		</p>
		<p>Password : <br>
		<input type="password" name="password"></p>
		<p><button type="submit">Submit</button></p>
		<?php echo form_close(); ?> -->

		<body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>assets/index2.html"><b>e-POKIR</b></a>
        <!-- <p><h3> Elektronik Pokok Pikiran </h3></p> -->
      </div>

       <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">
          Log in to e-POKIR
        </p>

        <form action="<?php echo base_url('Auth/cek_login'); ?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <!-- <div class="row"> -->
            <!-- <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div> -->
            <div class="col-xs-offset-8 col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
              <?php echo form_close(); ?>
            </div>
          </div>
        </form>

       </div>
      <!-- /.login-box-body -->
      <!-- <?php
        echo show_err_msg($this->session->flashdata('error_msg'));
      ?> -->
    </div>


</body>
</html>