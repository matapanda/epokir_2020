<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table, th, td {
  border: 1px solid black;
  width: auto;
}
	</style>
</head>
<body>
<h1 align="center">Data Usulan Pokok Pikiran</h1>
<table style="border-collapse: collapse;" align="center">
								
									<tr align="center">
										<th>No</th>
					                    <th>Usulan</th>
					                    <th>Volume</th>
					                    <th>Lokasi</th>
					                    <th>Tanggal Usulan</th>
					                    <th>Perangkat Daerah</th>
					                    <th>Anggota DPRD</th>
					                    <th>Dapil</th>
					                    <!-- <th>Foto Aspirasi</th> -->
					                    <th>Keterangan</th>
					                
					            		<th>Status Aspirasi</th>
										<!-- <th style="text-align: center;">Aksi</th> -->
									</tr>
							
								
									<?php $i=0; ?>
									<?php foreach ($aspirasi as $aspirasi): ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
										<td><?php echo $aspirasi->volume." ".$aspirasi->nama_satuan ?></td>
										
										<td><?php echo $aspirasi->lokasi ?></td>
										<td><?php echo date('d-m-Y', strtotime($aspirasi->tanggal)); ?></td>
										<td><?php echo $aspirasi->nama_perangkat ?></td>
										<td><?php echo $aspirasi->nama_dprd ?></td>
										<td><?php echo $aspirasi->nama_dapil ?></td>
										<!-- <td><img width="200px" height="100px" src="<?php print_r(base_url());?>upload/aspirasi/<?php echo $aspirasi->foto_as ?>"></td> -->
										<td><?php echo $aspirasi->keterangan ?></td>
										<td><?php echo $aspirasi->status ?></td>
										
									</tr>
									<?php endforeach; ?>

							
</table>