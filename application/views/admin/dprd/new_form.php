<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
 

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/dprd/') ?>"><i class="fas fa-arrow-left"></i> Kembali</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/dprd/add') ?>" method="post" enctype="multipart/form-data" >
							
							<!-- <div class="form-group">
								<label for="name">ID DPRD</label>
								<input class="form-control <?php echo form_error('id') ? 'is-invalid':'' ?>"
								 type="number" name="id"   />
								<div class="invalid-feedback">
									<?php echo form_error('id') ?>
								</div>
							</div> -->

							<div class="form-group">
								<label for="name">Nama*</label>
								<input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>"
								 type="text" name="nama" placeholder="Nama Anggota DPRD" />
								<div class="invalid-feedback">
									<?php echo form_error('nama') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="idfraksi">Fraksi*</label>
								<!-- new -->
								<select name="id_fraksi" class="form-control" required="required">
									<option value="" selected="selected">Pilih Fraksi</option>
									<?php foreach($fraksi as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama_fraksi']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="iddapil">Dapil*</label>
								<!-- new -->
								<select name="id_dapil" class="form-control" required="required">
									<option value="" selected="selected">Pilih Dapil</option>
									<?php foreach($dapil as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="jenis_kelamin">Jenis Kelamin*</label>
								<select name="jenis_kelamin" class="form-control" required="required">
									<option value="" selected="selected">Pilih Jenis Kelamin</option>
									<option value="Laki-laki">Laki-laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
							</div>

							<!-- <?php echo form_open($action); ?>
            				<div class="form-group">
                				<label for="foto">Kelamin</label>
               					<?php
                				$dd_kelamin_attribute = 'class="form-control select2"';
                				echo form_dropdown('kelamin', $dd_kelamin, $kelamin_selected, $dd_kelamin_attribute);
            			    	?>
            				</div> -->
       
        <!--jquery dan select2-->
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/select2/js/select2.min.js') ?>"></script>
        <script>
            $(document).ready(function () {
                $(".select2").select2({
                    placeholder: "Please Select"
                });
            });
        </script>
  

							<div class="form-group">
								<label for="notelp">No. Telepon*</label>
								<input class="form-control <?php echo form_error('notelp') ? 'is-invalid':'' ?>"
								 type="text" name="notelp"  placeholder="Nomor Telepon" />
								<div class="invalid-feedback">
									<?php echo form_error('notelp') ?>
								</div>
							</div>


							<div class="form-group">
								<label for="foto">Foto</label>
								<br>
								<i style="font-size: 12px;">Foto dalam bentuk PNG ukuran 3x4</i>
								<input class="form-control <?php echo form_error('foto') ? 'is-invalid':'' ?>"
								 type="file" name="foto"  placeholder="Foto" />
								<div class="invalid-feedback">

									<?php echo form_error('foto') ?>
								</div>
							</div>



							 <!-- <div class="form-group">
								<label for="name">Photo</label>
								<input class="form-control-file <?php echo form_error('price') ? 'is-invalid':'' ?>"
								 type="file" name="image" />
								<div class="invalid-feedback">
									<?php echo form_error('image') ?>
								</div>
							</div> -->
						<!--
							<div class="form-group">
								<label for="name">Description*</label>
								<textarea class="form-control <?php echo form_error('description') ? 'is-invalid':'' ?>"
								 name="description" placeholder="Product description..."></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('description') ?>
							</div>
						</div> -->

							<input class="btn btn-success" type="submit" name="btn" value="Simpan" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* wajib diisi 
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>