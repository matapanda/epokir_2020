<!DOCTYPE html>
<html lang="en">
 
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

					<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="col-md-4">
					<form method="post" enctype="multipart/form-data" action="dprd.php">
						
						<!-- <div class="card-header"> -->

									<a href="<?php echo site_url('admin/dprd/add') ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Tambah Baru</a>
									
        				<a href="<?php echo base_url('admin/dprd/export'); ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>

        			
</div>
<br>

				<!-- DataTables -->
				<div class="card mb-3">
			<!-- 		<div class="card-header">
				<a href="<?php echo site_url('admin/dprd/add') ?>"><i class="fas fa-plus"></i> Tambah Baru</a>
			</div>
						<form method="post" enctype="multipart/form-data" action="dprd.php"> -->
				<!-- <div class="card mb-3"> -->
					<!-- <div class="card-header">
					        				<a href="<?php echo base_url('admin/dprd/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a> -->
        				<!-- <a href="<?php echo base_url('admin/dprd/import'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> importtt Data Excel</a>
        				<button class="form-control btn btn-default" data-toggle="modal" data-target="#import-dprd"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button> -->
    		<!-- 	</div> -->
    			

					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" >
								<thead>
									<tr align="center">
										<th>No</th>
										<th>Nama</th>
										<th>Dapil</th>
										<th>Fraksi</th>
										<th>Jenis Kelamin</th>
										<th>No. Telepon</th>
										<th>Foto</th>
										<th style="text-align: center;">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; ?>
									<?php foreach ($dprd as $dprd): ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td style="min-width:200px;"><?php echo $dprd->nama ?></td>
										<td><?php echo $dprd->nama_dapil ?></td>
										<td><?php echo $dprd->nama_fraksi ?></td>
										<td><?php echo $dprd->jenis_kelamin ?></td>
										<td><?php echo $dprd->notelp ?></td>
										<td>
											<img src="<?php echo base_url('upload/product/'.$dprd->foto) ?>" width="64" />
										</td>
										<td align="center">
											<a href="<?php echo site_url('admin/dprd/edit/'.$dprd->id) ?>"
											 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/dprd/delete/'.$dprd->id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>


	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	


	</script>

	

<!-- <div id="tempat-modal"></div>

	<?php
  $data['judul'] = 'dprd';
  $data['url'] = 'admin/dprd/import';
  echo show_my_modal('modals/modal_import', 'import-dprd', $data);
?> -->

	

</body>

</html>