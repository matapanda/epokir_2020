<?php if($this->session->userdata('logged_in') != "Sudah Login"){
      redirect(base_url("auth"));
    }?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">

<?php $this->load->view("admin/_partials/navbar.php") ?>

<div id="wrapper">

  <?php $this->load->view("admin/_partials/sidebar.php") ?>

  <div id="content-wrapper">

    <div class="container-fluid">

        <!-- 
        karena i//ni halaman overview (home), kita matikan partial breadcrumb.
        Jika anda ingin mengampilkan breadcrumb di halaman overview,
        silahkan hilangkan komentar (//) di tag PHP di bawah.
        -->
    <?php //$this->load->view("admin/_partials/breadcrumb.php") ?>

    <!-- Icon Cards-->
    <div align="center">
    <img src="<?php echo base_url('assets/img/core-img/b.png') ?>" width="400px">
    </div>
      <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-home"></i>
            Beranda</div>
          <div class="card-body">
            <p>Selamat Datang Admin Halaman ini digunakan untuk merubah data dan mengetahui aspirasi yang dimasukkan oleh Anggota DPRD</p>
             <div class="row">
      <div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-info o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-city"></i>
        </div>
        <div class="mr-5">Daerah Pilihan</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/dapil">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
            <div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-primary o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-flag"></i>
        </div>
        <div class="mr-5">Fraksi</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/fraksi">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
      <div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-success o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-building"></i>
        </div>
        <div class="mr-5">Perangkat Daerah</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/perangkat_daerah">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
           <div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-warning o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-box"></i>
        </div>
        <div class="mr-5">Satuan</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/satuan">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
      <div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-danger o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-users"></i>
        </div>
        <div class="mr-5">Users</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/user">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
<!-- keluar -->
<div class="col-xl-2 col-sm-6 mb-3">
      <div class="card text-white bg-secondary o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-users"></i>
        </div>
        <div class="mr-5">Admin Sekwan</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="http://localhost/epokir/admin/c_admin/logout">
        <span class="float-left">Keluar</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
      
    </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>
  
        <div class="row">
        <div class="col-lg-6">
         <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-list"></i>
            Aspirasi Terbaru</div>
          <div class="card-body">
            <div class="col-lg-6">
      <div class="card text-white bg-warning o-hidden h-100">
        <div class="card-body" align="center">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-list"></i>
        </div>
        <div class="mr-5">Aspirasi/Usulan</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/aspirasi">
        <span class="float-left">Lihat Detail Informasi</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
      <br>
              <div class="table-responsive">
              <table class="table table-bordered table-striped" id="dataTable" >
                <thead>
                  <tr align="center">
                    <th>No</th>
                              <th>Usulan</th>
                            
                              <th>Anggota DPRD</th>
                              
                    <!-- <th style="text-align: center;">Aksi</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; ?>
                  <?php foreach ($aspirasi as $aspirasi): ?>
                  <tr>
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
                  
                    <td><?php echo $aspirasi->nama_dprd ?></td>
                  
                    <!-- <td width="250" align="center">
                      <a href="<?php echo site_url('admin/aspirasi/edit/'.$aspirasi->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/aspirasi/delete/'.$aspirasi->id) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td> -->
                    
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>

          </div>
          <div class="card-footer small text-muted"></div>
        </div>
    </div>

    <div class="col-lg-6">
         <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-user"></i>
            Anggota DPRD</div>
          <div class="card-body">
                <div class="col-lg-6">
      <div class="card text-white bg-primary o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-user"></i>
        </div>
        <div class="mr-5">Anggota DPRD</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="admin/dprd">
        <span class="float-left">Lihat Detail Informasi</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
      <br>
              <div class="table-responsive">
              <table class="table table-bordered table-striped" id="dataTable" >
                <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Nama</th>
                    
                    <th>Foto</th>
                   
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; ?>
                  <?php foreach ($dprd as $dprd): ?>
                  <tr>
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:200px;"><?php echo $dprd->nama ?></td>
                   
                    <td>
                      <img src="<?php echo base_url('upload/product/'.$dprd->foto) ?>" width="64" />
                    </td>
            
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>

          </div>
          <div class="card-footer small text-muted"></div>
        </div>
    </div>
    </div>
  <!--   <div id="content-wrapper">
      <div class="container-fluid">
        <img src="<?=base_url()?>assets/b.jpg" width="1300px" height="550px">
      
      </div>
    </div> -->

    <!-- Area Chart Example-->
    <!-- <div class="card mb-3">
      <div class="card-header">
      <i class="fas fa-chart-area"></i>
      Visitor Stats</div>
      <div class="card-body">
      <canvas id="myAreaChart" width="100%" height="30"></canvas>
      </div>
      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div> -->

    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
    <?php $this->load->view("admin/_partials/footer.php") ?>

  </div>
  <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>