<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

					<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
						<?php endif; ?>

					<div class="col-md-4">
					<form method="post" enctype="multipart/form-data" action="user.php">
						
						<!-- <div class="card-header"> -->

								<a href="<?php echo site_url('admin/satuan/add') ?>""  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Tambah Baru</a>

        			<!-- 	<a href="<?php echo base_url('admin/dapil/export'); ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a> -->

        				
</div>
<br>

				<!-- DataTables -->
				<div class="card mb-3">
					<!-- <div class="card-header">
						<a href="<?php echo site_url('admin/satuan/add') ?>"><i class="fas fa-plus"></i> Tambah Baru</a>
					</div>
					<form method="post" enctype="multipart/form-data" action="Satuan.php">
									<div class="card mb-3">
					<div class="card-header"> -->
        				<!-- <a href="<?php echo base_url('admin/volume/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>
        				 -->
        				<!-- <a href="<?php echo base_url('admin/dprd/import'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> importtt Data Excel</a>
        				<button class="form-control btn btn-default" data-toggle="modal" data-target="#import-dprd"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button> -->
    		<!-- 	</div> -->
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr align="center">
										<th>No</th>
										<th>Nama Satuan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; ?>
									<?php foreach ($satuan as $satuan): ?>
									<tr align="center">
										<td>
											<?php echo ++$i; ?>
										</td>
										<td>
											<?php echo $satuan->nama_satuan ?>
										</td>
										<!-- <td>
											<img src="<?php echo base_url('upload/product/'.$product->image) ?>" width="64" />
										</td>
										<td class="small">
											<?php echo substr($product->description, 0, 120) ?>...</td> -->
										<td width="250" align="center">
											<a href="<?php echo site_url('admin/satuan/edit/'.$satuan->id) ?>"
											 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/satuan/delete/'.$satuan->id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

</body>

</html>