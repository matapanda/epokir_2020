<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<!-- Card  -->
				<div class="card mb-3">
					<div class="card-header">

						<a href="<?php echo site_url('admin/user/') ?>"><i class="fas fa-arrow-left"></i>
							Kembali</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/user/edit') ?>" method="post" enctype="multipart/form-data">

							<input type="hidden" name="id" value="<?php echo $user->id?>" />

							<div class="form-group">
								<label for="id">Nama*</label>
								<select name="id_dprd" class="form-control" required="required">
									<?php foreach($dprd as $data) { ?>
									<option value="<?php echo $data['id']; ?>" <?php echo ($user->id_dprd == $data['id'] ? 'selected="selected"' : ''); ?>><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div>


							<!-- <div class="form-group">
								<label for="id">Nama</label>
								<select name="id_dprd" class="form-control" required="required">
									<option value="" selected="selected">Pilih DPRD</option>
									<?php foreach($dprd as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div> -->


							<div class="form-group">
								<label for="username">Username*</label>
								<input class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>"
								 type="text" name="username"  placeholder="Username" value="<?php echo $user->username ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('username') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="password">Password*</label>
								<input class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>"
								 type="password" name="password"  placeholder="Password" value=""/>
								<div class="invalid-feedback">
									<?php echo form_error('password') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="level">Level*</label>
								<select name="level" class="form-control" required="required">
									<option value="" selected="selected">Pilih Jenis User</option>
									<option value="admin">Admin</option>
									<option value="dprd">DPRD</option>
								</select>
							</div>

							<!-- <div class="form-group">
								<label for="foto">Foto*</label>
								<input class="form-control-file <?php echo form_error('foto') ? 'is-invalid':'' ?>"
								 type="file" name="image" value="<?php echo $user->foto ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('foto') ?>
								</div>
							</div> -->

							<input class="btn btn-success" type="submit" name="btn" value="SImpan" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>