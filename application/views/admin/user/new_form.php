<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/user/') ?>"><i class="fas fa-arrow-left"></i>Kembali</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/user/add') ?>" method="post" enctype="multipart/form-data" >
							
							<!-- <div class="form-group">
								<label for="name">No*</label>
								<input class="form-control <?php echo form_error('id') ? 'is-invalid':'' ?>"
								 type="number" name="id" readonly="readonly" />
								<div class="invalid-feedback">
									<?php echo form_error('id') ?>
								</div>
							</div> -->

							<div class="form-group">
								<label for="id">Nama*</label>
								<select name="id_dprd" class="form-control" required="required">
									<option value="" selected="selected">Pilih DPRD</option>
									<?php foreach($dprd as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="username">Username*</label>
								<input class="form-control <?php echo form_error('username') ? 'is-invalid':'' ?>"
								 type="text" name="username"  placeholder="Username" />
								<div class="invalid-feedback">
									<?php echo form_error('username') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="password">Password*</label>
								<input class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>"
								 type="password" name="password"  placeholder="Password" />
								<div class="invalid-feedback">
									<?php echo form_error('password') ?>
								</div>
							</div>

							<!-- <div class="form-group">
								<label for="level">Level*</label>
								<input class="form-control <?php echo form_error('level') ? 'is-invalid':'' ?>"
								 type="text" name="level"  placeholder="Level" />
								<div class="invalid-feedback">
									<?php echo form_error('level') ?>
								</div>
							</div> -->

							<div class="form-group">
								<label for="level">Level*</label>
								<select name="level" class="form-control" required="required">
									<option value="" selected="selected">Pilih Jenis User</option>
									<option value="admin">Admin</option>
									<option value="dprd">DPRD</option>
								</select>
							</div>


<!-- 
							<div class="form-group">
								<label for="foto">Foto*</label>
								<input class="form-control-file <?php echo form_error('foto') ? 'is-invalid':'' ?>"
								 type="file" name="image" />
								<div class="invalid-feedback">
									<?php echo form_error('foto') ?>
								</div>
							</div> -->

							


							<input class="btn btn-success" type="submit" name="btn" value="Simpan" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* wajib diisi 
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/modal.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>