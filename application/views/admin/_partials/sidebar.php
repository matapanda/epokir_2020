<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin') ?>">
            <i class="fas fa-fw fa-home"></i>
            <span>Beranda</span>
        </a>
    </li>

     <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin/aspirasi') ?>"> 
            <i class="fas fa-fw fa-list"></i>
            <span>Aspirasi</span>
        </a>
    </li>


 <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-archive"></i>
            <span>Manajemen Data</span>
        </a>

        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <!--       <a class="dropdown-item" href="<?php echo site_url('admin/dapil/add') ?>">  Tambah Dapil</a> -->
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil') ?>"><i class="fas fa-fw fa-city"></i> Data Dapil</a>
             <a class="dropdown-item" href="<?php echo site_url('admin/fraksi') ?>"><i class="fas fa-fw fa-flag"></i> Data Fraksi</a>
              <a class="dropdown-item" href="<?php echo site_url('admin/dprd') ?>">  <i class="fas fa-fw fa-user"></i> Data DPRD</a>
               <a class="dropdown-item" href="<?php echo site_url('admin/perangkat_daerah') ?>">   <i class="fas fa-fw fa-building"></i> Data Perangkat</a>
                  <a class="dropdown-item" href="<?php echo site_url('admin/user') ?>"> <i class="fas fa-fw fa-users"></i> Data User</a>
                  <a class="dropdown-item" href="<?php echo site_url('admin/satuan') ?>"> <i class="fas fa-fw fa-box"></i> Data Satuan</a>
        </div>
    </li>


  <!--   <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-city"></i>
            <span>Daerah Pilihan</span>
        </a>

        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil/add') ?>">  <i class="fas fa-fw fa-city"></i> Tambah Dapil</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil') ?>">Daftar Dapil</a>
        </div>
    </li>


    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'fraksi' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-flag"></i>
            <span>Fraksi</span>
        </a>
        
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/fraksi/add') ?>">Tambah Fraksi</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/fraksi') ?>">Daftar Fraksi</a>
        </div>
    </li>


     <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dprd' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Anggota DPRD</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/dprd/add') ?>">Tambah DPRD</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/dprd') ?>">Daftar DPRD</a>
        </div>
    </li>


    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'perangkat_daerah' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-building"></i>
            <span>Perangkat Daerah</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/perangkat_daerah/add') ?>">Tambah</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/perangkat_daerah') ?>">Daftar Perangkat</a>
        </div>
    </li>

    

    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'user' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/user/add') ?>">Tambah Users</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/user') ?>">Daftar Users</a>
        </div>
    </li>

    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'satuan' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-box"></i>
            <span>Satuan</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/satuan/add') ?>">Tambah Satuan</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/satuan') ?>">Daftar Satuan</a>
        </div>
    </li>

 -->
   



    <!-- <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li> -->
</ul>