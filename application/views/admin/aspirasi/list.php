<?php if($this->session->userdata('logged_in') != "Sudah Login"){
      redirect(base_url("auth"));
    }?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
						<?php endif; ?>

				<div class="col-md-4">
					<form method="post" enctype="multipart/form-data" action="Aspirasi.php">
						
						<!-- <div class="card-header"> -->
        				<!-- <a href="<?php echo base_url('admin/aspirasi/export'); ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a> -->

        					<a href="<?php echo base_url('admin/aspirasi/cetak'); ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Print PDF</a>
</div>
<br>

				<!-- DataTables -->
				<div class="card mb-3">
					<!-- <div class="card-header">port
						<a href="<?php echo site_url('admin/aspirasi/add') ?>"><i class="fas fa-plus"></i> Tambah Baru</a>
					</div> -->
					
					<!-- <br>
						<br>
						<a href="<?php echo base_url('admin/aspirasi/export'); ?>" class="form-control btn btn-print">Print</a> -->

        				<!-- <a href="<?php echo base_url('admin/dprd/import'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> importtt Data Excel</a>
        				<button class="form-control btn btn-default" data-toggle="modal" data-target="#import-dprd"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button> -->
    			<!-- </div> -->
					<div class="card-body">
			 <div id="toolbar">
        <!--      <a href="print.php" target="blank"><button class="btn btn-success"; data-toggle="modal"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;</button></a>
        			<form method="post" enctype="multipart/form-data" action="Aspirasi.php">
         <a href="<?php echo base_url('admin/aspirasi/export');?>"><button class="btn btn-success"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Export Data Excel</button></a></form>
                    </div>
                    <br> -->


						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" >
								<thead>
									<tr align="center">
										<th>No</th>
					                    <th>Usulan</th>
					                    <th>Volume</th>
					                    <th>Lokasi</th>
					                    <th>Tanggal Usulan</th>
					                    <th>Perangkat Daerah</th>
					                    <th>Anggota DPRD</th>
					                    <th>Foto Aspirasi</th>
					                    <th>Dapil</th>
					                    <th>Ket</th>
					                    <th>Status</th>
										<!-- <th style="text-align: center;">Aksi</th> -->
									</tr>
								</thead>
								<tbody>
									<?php $i=0; ?>
									<?php foreach ($aspirasi as $aspirasi): ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
										<td><?php echo $aspirasi->volume." ".$aspirasi->nama_satuan ?></td>
										
										<td><?php echo $aspirasi->lokasi ?></td>
										<td><?php echo date('d-m-Y', strtotime($aspirasi->tanggal)); ?></td>
										<td><?php echo $aspirasi->nama_perangkat ?></td>
										<td><?php echo $aspirasi->nama_dprd ?></td>
										<td><img src="<?php echo base_url('upload/aspirasi/'.$aspirasi->foto_aspirasi) ?>" width="64" /></td>
										<td><?php echo $aspirasi->nama_dapil ?></td>
										<td><?php echo $aspirasi->keterangan ?></td>
										<td align="center" style="min-width:90px;">
											<?php if($aspirasi->status == 'Belum Dikonfirmasi') { ?>
											<a href="<?php echo base_url(); ?>admin/aspirasi/konfirmasi/<?php echo $aspirasi->id; ?>/Diterima" type="button" 20px;" class="btn btn-success"><i class="fa fa-check" style="size: 20px;"></i></a>


											<a href="<?php echo base_url(); ?>admin/aspirasi/konfirmasi/<?php echo $aspirasi->id; ?>/Ditolak" type="button" 20px;" class="btn btn-danger"><i class="fa fa-times"  style="size: 20px;"></i></a>
											<?php } else { ?>
											
											<div class="label label-primary">	<?php 
													$sts = $aspirasi->status; ?></div>

							<?php
                            if($sts=='Diterima'){
                            echo "<button class=\"btn btn-success btn-xs\">Diterima</button>";
                            }else if($sts=='Ditolak'){
                            echo "<button class=\"btn btn-danger btn-xs\">&nbsp;Ditolak&nbsp;&nbsp;</button>";    
                            }else{
                              echo "";
                            }
                            ?>
											<?php } ?>	
										</td>
										<!-- <td width="250" align="center">
											<a href="<?php echo site_url('admin/aspirasi/edit/'.$aspirasi->id) ?>"
											 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/aspirasi/delete/'.$aspirasi->id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td> -->
										
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

</body>

</html>