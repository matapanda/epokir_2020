<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table, th, td {
  border: 1px solid black;
  width: auto;
}
	</style>
</head>
<body>
<h1>Data Usulan Pokok Pikiran</h1>
<table width="100%" style="border-collapse: collapse;">
								
									<tr>
										<th width="3%">No</th>
					                    <th width="25%">Usulan</th>
					                    <th width="10%">Volume</th>
					                    <th width="15%">Lokasi</th>
					    
					                    <th width="15%">Perangkat Daerah</th>
					                    <th width="10%">Anggota DPRD</th>
					                    <th width="7%">Dapil</th>
										<th width="10%">Keterangan</th>
										<th width="5%">Status</th>					                
					            
										<!-- <th style="text-align: center;">Aksi</th> -->
									</tr>
							
								
									<?php $i=0; ?>
									<?php foreach ($aspirasi as $aspirasi): ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td><?php echo $aspirasi->usulan ?></td>
										<td><?php echo $aspirasi->volume." ".$aspirasi->nama_satuan ?></td>

										<td><?php echo $aspirasi->lokasi ?></td>
										<td><?php echo $aspirasi->nama_perangkat ?></td>
										<td><?php echo $aspirasi->nama_dprd ?></td>
										<td><?php echo $aspirasi->nama_dapil ?></td>

										<td><?php echo $aspirasi->keterangan ?></td>
										
										
										<?php
											$str_status = "";
											if($aspirasi->status == 'Belum Dikonfirmasi') {
												$str_status = "Belum Dikonfirmasi";
											} else { 
											    $sts = $aspirasi->status;
											    if($sts=='Diterima'){
											    	$str_status = 'Diterima';
											    }else if($sts=='Ditolak'){
											    	$str_status = 'Ditolak';
											    }else{
											    	$str_status = '';
											    }
											}
										?>

										<td><?php echo $str_status ?></td>
										
									</tr>
									<?php endforeach; ?>

							
</table>

</body>
<script type="text/javascript">
	
	window.print();
</script>
</html>

