<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
  <title>E-POKIR</title>

    <!-- Favicon -->
      <link rel="icon" href="<?php echo base_url('assets/img/core-img/favicon.ico') ?>">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url('test/style.css') ?>">


</head>

<style >
body {
  margin: 0;
  background: linear-gradient(-100deg, #fff, #fff);
  font-family: sans-serif;
  font-weight: 100;
}

/*.container {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}*/

table {
  width: 1000px;
  border-collapse: collapse;
  overflow: hidden;
  box-shadow: 0 0 50px rgba(70,0,40,0.1);
  text-decoration-color: #000;

th,
td {
  padding: 15px;
  background-color: rgba(77,255,255,0.2);
  color: #fff;
}

th {
  text-align: left;
}

thead {
  th {
    background-color: #55608f;
  }
}

tbody {
  tr {
    &:hover {
      background-color: rgba(255,255,255,0.3);
    }
  }
  td {
    position: relative;
    &:hover {
      &:before {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        top: -9999px;
        bottom: -9999px;
        background-color: rgba(255,255,255,0.2);
        z-index: -1;
      }
    }
  }
}

</style>



<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Navbar Area -->
        <div class="fitness-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="fitnessNav">

                        <!-- Nav brand -->
                        <a href="<?php echo site_url('masyarakat/overview/index') ?>" class="nav-brand"><img width="350px" height="100px"src="<?php echo base_url('assets/img/core-img/b.png') ?>" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                     <li><a href="<?php echo site_url('masyarakat/overview/index') ?>">Beranda</a></li>
                                   <!--  <li><a href="#">Pages</a>
                                       <ul class="dropdown">
                                           <li><a href="index.html">Home</a></li>
                                           <li><a href="about-us.html">About Us</a></li>
                                           <li><a href="services.html">Services</a></li>
                                           <li><a href="blog.html">News</a></li>
                                           <li><a href="contact.html">Contact</a></li>
                                           <li><a href="elements.html">Elements</a></li>
                                       </ul>
                                   </li>
                                   <li><a href="#">Mega Menu</a>
                                       <div class="megamenu">
                                           <ul class="single-mega cn-col-4">
                                               <li><a href="index.html">Home</a></li>
                                               <li><a href="about-us.html">About Us</a></li>
                                               <li><a href="services.html">Services</a></li>
                                               <li><a href="blog.html">News</a></li>
                                               <li><a href="contact.html">Contact</a></li>
                                               <li><a href="elements.html">Elements</a></li>
                                           </ul>
                                           <ul class="single-mega cn-col-4">
                                               <li><a href="index.html">Home</a></li>
                                               <li><a href="about-us.html">About Us</a></li>
                                               <li><a href="services.html">Services</a></li>
                                               <li><a href="blog.html">News</a></li>
                                               <li><a href="contact.html">Contact</a></li>
                                               <li><a href="elements.html">Elements</a></li>
                                           </ul>
                                           <ul class="single-mega cn-col-4">
                                               <li><a href="index.html">Home</a></li>
                                               <li><a href="about-us.html">About Us</a></li>
                                               <li><a href="services.html">Services</a></li>
                                               <li><a href="blog.html">News</a></li>
                                               <li><a href="contact.html">Contact</a></li>
                                               <li><a href="elements.html">Elements</a></li>
                                           </ul>
                                           <ul class="single-mega cn-col-4">
                                               <li><a href="index.html">Home</a></li>
                                               <li><a href="about-us.html">About Us</a></li>
                                               <li><a href="services.html">Services</a></li>
                                               <li><a href="blog.html">News</a></li>
                                               <li><a href="contact.html">Contact</a></li>
                                               <li><a href="elements.html">Elements</a></li>
                                           </ul>
                                       </div>
                                   </li> -->
                                 <!--    <li><a href="<?php echo site_url('masyarakat/overview/index') ?>#adalah">Tentang</a></li> -->
                                    <li><a href="<?php echo site_url('masyarakat/overview/about') ?>">DPRD</a></li>
                                    <li class ="nav-tabs"><a href="<?php echo site_url('masyarakat/overview/aspirasi/') ?>">Usulan</a></li>
                                </ul>

                                <!-- Call Button -->
                                <a href="<?php echo site_url('auth') ?>" align="center" class="fitness-btn menu-btn ml-30">Masuk</a>


                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url('<?php echo base_url('assets/img/bg-img/log1.jpg') ?>');">
        <div class="bradcumbContent">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        
  <h2 class="text-center">USULAN ANGGOTA DPRD </h2>
                           <br>
    <br><br>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Blog Area Start ##### -->
    <!-- <div class="blog-area mt-50 section-padding-100" align="center">
        <div class="container" >
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="fitness-blog-posts">
                        <div class="row"> -->

<!-- > -->
<div class="container">
<br>

<!-- 
<div class="card-body"> -->

  

            <!-- <div class=""> -->
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example" >
                <thead>
                  <tr align="center">
                    <th>No</th>
                    <th>Usulan</th>
                    <th>Volume</th>
                    <th>Lokasi</th>
                    <th>Tanggal Usulan</th>
                    <th>Perangkat Daerah</th>
                    <th>Anggota DPRD</th>
                    <th>Dapil</th>
                    <th>Ket</th>
                  </tr>
                </thead>


                 <tbody>
                  <?php $i=0; ?>
                  <?php foreach ($aspirasi as $aspirasi): ?>
                  <tr align="left">
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
                    <td><?php echo $aspirasi->volume." ".$aspirasi->nama_satuan ?></td>
                    
                    <td style="min-width:137px;"><?php echo $aspirasi->lokasi ?></td>
                    <td style="min-width:90px;" align="center"><?php echo date('d-m-Y', strtotime($aspirasi->tanggal)); ?></td>
                    <td><?php echo $aspirasi->nama_perangkat ?></td>
                    <td><?php echo $aspirasi->nama_dprd ?></td>
                    <td style="min-width:80px;"><?php echo $aspirasi->nama_dapil ?></td>
                    <td style="min-width:30px;"><?php echo $aspirasi->keterangan ?></td>

                    
                  </tr>
                  <?php endforeach; ?>
                
                </tbody> 
              </table>
            <!-- </div> -->
          </div>
        </div>
        




   
<br>
<br>
<br>
<br>

                            <!-- Single Blog Start
                            <!-- <div class="col-12">
                                <div class="single-blog-post mb-100">
                                    Post Thumb
                                    <div class="blog-post-thumb mb-30">
                                        <img src="img/blog-img/1.png" alt="">
                                        <div class="post-date">
                                            <p><span>May</span>10</p>
                                        </div>
                                    </div>
                                    Post Title
                                    <a href="#" class="post-title">10 Healthy foods for a good living</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                    Post Excerpt
                                    <p>Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id. Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id.</p>
                                </div>
                            </div>
                            
                            Single Blog Start
                            <div class="col-12">
                                <div class="single-blog-post mb-100">
                                    Post Thumb
                                    <div class="blog-post-thumb mb-30">
                                        <img src="img/blog-img/2.png" alt="">
                                        <div class="post-date">
                                            <p><span>May</span>10</p>
                                        </div>
                                    </div>
                                    Post Title
                                    <a href="#" class="post-title">Why should we all do sports?</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                    Post Excerpt
                                    <p>Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id. Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id.</p>
                                </div>
                            </div>
                            
                            Single Blog Start
                            <div class="col-12">
                                <div class="single-blog-post mb-100">
                                    Post Thumb
                                    <div class="blog-post-thumb mb-30">
                                        <img src="img/blog-img/3.png" alt="">
                                        <div class="post-date">
                                            <p><span>May</span>10</p>
                                        </div>
                                    </div>
                                    Post Title
                                    <a href="#" class="post-title">10 Healthy foods for a good living</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                    Post Excerpt
                                    <p>Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id. Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id.</p>
                                </div>
                            </div>
                            
                                                    </div>
                                                </div>
                            
                                                Pagination
                                                <div class="fitness-pagination-area">
                                                    <nav>
                            <ul class="pagination">
                                <li class="page-item active"><a class="page-link" href="#">01.</a></li>
                                <li class="page-item"><a class="page-link" href="#">02.</a></li>
                                <li class="page-item"><a class="page-link" href="#">03.</a></li>
                            </ul>
                                                    </nav>
                                                </div>
                                            </div>
                            
                                            <div class="col-12 col-md-4">
                                                <div class="fitness-blog-sidebar">
                            
                                                    Add Widget
                                                    <div class="add-widget mb-100">
                            <a href="#"><img src="img/bg-img/add3.png" alt=""></a>
                                                    </div>
                            
                                                    Blog Post Catagories
                                                    <div class="blog-post-categories mb-100">
                            <h5>Categories</h5>
                            <ul>
                                <li><a href="#"><span>Fitness</span> <span>(9)</span></a></li>
                                <li><a href="#"><span>Health</span> <span>(11)</span></a></li>
                                <li><a href="#"><span>Outdoor Sports</span> <span>(21)</span></a></li>
                                <li><a href="#"><span>Lifestyle</span> <span>(14)</span></a></li>
                                <li><a href="#"><span>Uncategorized</span> <span>(7)</span></a></li>
                            </ul>
                                                    </div>
                            
                                                    Latest Blog Posts Area
                                                    <div class="latest-blog-posts mb-100">
                            <h5>Latest Posts</h5>
                            
                            Single Latest Blog Post
                            <div class="single-latest-blog-post">
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/4.png" alt="">
                                </div>
                                <div class="post-content">
                                    Post Title
                                    <a href="#" class="post-title">10 Healthy foods for a good living</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                </div>
                            </div>
                            
                            Single Latest Blog Post
                            <div class="single-latest-blog-post">
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/5.png" alt="">
                                </div>
                                <div class="post-content">
                                    Post Title
                                    <a href="#" class="post-title">10 Healthy foods for a good living</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                </div>
                            </div>
                            
                            Single Latest Blog Post
                            <div class="single-latest-blog-post">
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/6.png" alt="">
                                </div>
                                <div class="post-content">
                                    Post Title
                                    <a href="#" class="post-title">10 Healthy foods for a good living</a>
                                    Post Meta
                                    <div class="post-meta">
                                        <p>By <a href="#">Admin</a> | in <a href="#">Health</a> | <a href="#">3 comments</a></p>
                                    </div>
                                </div>
                            </div>
                            
                                                    </div>
                            
                                                    Add Widget
                                                    <div class="add-widget">
                            <a href="#"><img src="img/bg-img/add4.png" alt=""></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                ##### Blog Area End #####
                            
                                ##### Footer Area Start #####
                                <footer class="footer-area section-padding-100-0 bg-img bg-overlay" style="background-image: url(img/bg-img/bg-11.jpg);">
                            
                                    <div class="main-footer-area">
                                        <div class="container">
                                            <div class="row">
                            
                                                Footer Widget Area
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="footer-widget-area mb-50">
                            Widget Title
                            <div class="widget-title">
                                <a href="#"><img src="img/core-img/logo.png" alt=""></a>
                            </div>
                            <p class="mb-30">Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id. Nullam vulputate a lectus non molestie. Duis at lobortis neque.</p>
                            Social Info
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                                                    </div>
                                                </div>
                            
                                                Footer Widget Area
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="footer-widget-area mb-50">
                            Widget Title
                            <div class="widget-title">
                                <h6>testimonials</h6>
                            </div>
                            
                            Testimonials Slides
                            <div class="testimonials-slides owl-carousel">
                            
                                <div class="single-slide">
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Michael Smith</span>
                                    </div>
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Julia Williams</span>
                                    </div>
                                </div>
                            
                                <div class="single-slide">
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Michael Smith</span>
                                    </div>
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Julia Williams</span>
                                    </div>
                                </div>
                            
                                <div class="single-slide">
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Donec molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Michael Smith</span>
                                    </div>
                                    Single Testimonial
                                    <div class="single-testimonial">
                                        <p>“ Molestie tincidunt tellus sit amet aliquet. Proin auctor nisi ut purus</p>
                                        <span>Julia Williams</span>
                                    </div>
                                </div>
                            
                            </div>
                                                    </div>
                                                </div>
                            
                                                Footer Widget Area
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="footer-widget-area mb-50">
                            <div class="widget-title">
                                <h6>Classes</h6>
                            </div>
                            
                            <nav>
                                <ul class="fitness-classes">
                                    <li><a href="#">Bodybuilding Class</a></li>
                                    <li><a href="#">Fitness Class</a></li>
                                    <li><a href="#">Yoga Courses</a></li>
                                    <li><a href="#">Dumbell Class</a></li>
                                    <li><a href="#">Spinning Class</a></li>
                                    <li><a href="#">Kangoo Jump Class</a></li>
                                </ul>
                            </nav>
                                                    </div>
                                                </div>
                            
                                                Footer Widget Area
                                                <div class="col-12 col-sm-6 col-lg-3">
                                                    <div class="footer-widget-area mb-50">
                            <div class="widget-title">
                                <h6>Contact</h6>
                            </div>
                            
                            Single Contact
                            <div class="single-contact mb-30">
                                <span>Address:</span>
                                <p>481 Creekside Lane Avila <br>Beach, CA 93424</p>
                            </div>
                            
                            Single Contact
                            <div class="single-contact mb-30">
                                <span>Phone:</span>
                                <p>+53 345 7953 32453</p>
                            </div>
                            
                            Single Contact
                            <div class="single-contact mb-30">
                                <span>Email:</span>
                                <p>yourmail@gmail.com</p>
                            </div>
                            
                                                    </div>
                                                </div>
                            
                                            </div>
                                        </div>
                                    </div>
                            
                                    Copywrite Area
                                    <div class="bottom-footer-area">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <p><a href="#">Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
 <footer class="footer-area section-padding-100-0 bg-img bg-overlay" style="background-image: url(img/bg-img/log2.jpg);">
    
        <div class="main-footer-area">
            <div class="container">
                <div class="row">
    
                  
                </div>
            </div>
        </div>
    
        Copywrite Area
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p><a href="#">
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> e-POKIR  Kota Malang <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">NCC Kota Malang</a> 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <?php $this->load->view("admin/_partials/js.php") ?>
    <!-- jQuery-2.2.4 js -->
  <script src="<?php echo base_url ('assets/js/jquery/jquery-2.2.4.min.js') ?>"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url('assets/js/bootstrap/popper.min.js') ?>"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url('assets/js/plugins/plugins.js') ?>"></script>
    <!-- Active js -->
    <script src="<?php echo base_url('assets/js/active.js') ?>"></script>


    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <!-- Live Chat Code :: Start of Tawk.to Script -->
    <script>
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            // s1.src = 'https://embed.tawk.to/5b55ea72df040c3e9e0bdf85/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();


      $(document).ready(function() {
        $('#example').DataTable();
      } );

    </script>
    <!-- End of Tawk.to Script -->
</body>

</html>