<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
  <title>E-POKIR</title>

    <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url('assets/img/core-img/favicon.ico') ?>">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('test/style.css') ?>">
    <style>
      .single-teachers-area .teachers-bg-gradients {
        background: #ffffff !important;
        border: 3px solid #222222 !important;
      }
      
    </style>
</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Navbar Area -->
        <div class="fitness-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="fitnessNav">

                        <!-- Nav brand -->
                          <a href="<?php echo site_url('masyarakat/overview/index') ?>" class="nav-brand"><img width="350px" height="100px" src="<?php echo base_url('assets/img/core-img/b.png') ?>" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                       <li><a href="<?php echo site_url('masyarakat/overview/index') ?>">Beranda</a></li>

                                   <!--  <li><a href="<?php echo site_url('masyarakat/overview/index') ?>#adalah">Tentang</a></li> -->
                                    <li class ="nav-tabs"><a href="<?php echo site_url('masyarakat/overview/about') ?>">DPRD</a></li>
                                    <li><a href="<?php echo site_url('masyarakat/overview/aspirasi/') ?>">Usulan</a></li>
                                </ul>

                                <!-- Call Button -->
                                <a href="<?php echo site_url('auth') ?>" align="center" class="fitness-btn menu-btn ml-30">Masuk</a>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->
   
       <div class="breadcumb-area bg-img bg-overlay" style="background-image: url('<?php echo base_url('assets/img/bg-img/log1.jpg') ?>');">
    
        <div class="bradcumbContent">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                             <h2 align="center">ANGGOTA DPRD</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                               <!--  <li class="breadcrumb-item"><a href="#">Home</a></li>
                               <li class="breadcrumb-item active" aria-current="page">Anggota DPRD</li> -->
                               <br><br><br>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Cool Facts Area End ##### -->

    <!-- ##### Team Area Start ##### -->
    <section class="teachers-area section-padding-100-0">
        <div class="container">
<!--             <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                       
                        <h2 align="center">Anggota DPRD Kota Malang</h2>
                    </div>
                </div>
            </div> -->

           
            <div class="row">
               <!-- <div class="container"> -->
              
                <!-- Single Teachers -->
                
                <?php $i=0; ?>
                  <?php foreach ($dprd as $dprd): ?>
                    <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-teachers-area mb-100">
                        <!-- Bg Gradients -->
                        <div class="teachers-bg-gradients"></div>
                        <!-- Thumbnail -->
                        <!-- <div class="teachers-thumbnail">
                            <img src="<?php echo base_url('assets/img/team-img/t1.png') ?>" alt="">
                        </div> -->
                        <div class="teachers-thumbnail text-center">
                            <img algn="center" style="width: 190px; height: 240px;" src="<?php echo base_url('upload/product/'.$dprd->foto) ?>"/>
                        </div>
                        <!-- Meta Info -->
                        <div class="teachers-info">
                            <h6><?php echo $dprd->nama ?></h6>
                           <a href="<?php echo site_url('masyarakat/overview/detail/'.$dprd->id) ?>"><span>Detail Anggota</span></div></a>
                    </div>
                </div>
                  <!-- <tr>
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:230px;"><?php echo $dprd->nama ?></td>
                    <td><?php echo $dprd->nama_dapil ?></td>
                    <td><?php echo $dprd->jenis_kelamin ?></td>
                    <td><?php echo $dprd->notelp ?></td>
                    <td>
                      <img src="<?php echo base_url('upload/product/'.$dprd->foto) ?>" width="64" />
                    </td>
                    <td width="250">
                      <a href="<?php echo site_url('admin/dprd/edit/'.$dprd->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/dprd/delete/'.$dprd->id) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                  </tr> -->
                  <?php endforeach; ?>
                
                <!-- <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-teachers-area mb-100"> -->
                        <!-- Bg Gradients -->
                        <!-- <div class="teachers-bg-gradients"></div> -->
                        <!-- Thumbnail -->
                        <!-- <div class="teachers-thumbnail">
                            <img src="<?php echo base_url('assets/img/team-img/t1.png') ?>" alt="">
                        </div> -->
                        <!-- Meta Info -->
                        <!-- <div class="teachers-info">
                            <h6>Khusnul Kris</h6>
                            <span>Dapil 5</span>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
    </section>
    <!-- ##### Team Area End ##### -->

     <footer class="footer-area section-padding-100-0 bg-img bg-overlay" style="background-image: url(img/bg-img/home1.jpg);">
    
        <div class="main-footer-area">
            <div class="container">
                <div class="row">
    
                  
                </div>
            </div>
        </div>
    
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p><a href="#">
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> e-POKIR  Kota Malang <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">NCC Kota Malang</a> 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url ('assets/js/jquery/jquery-2.2.4.min.js') ?>"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url('assets/js/bootstrap/popper.min.js') ?>"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url('assets/js/plugins/plugins.js') ?>"></script>
    <!-- Active js -->
    <script src="<?php echo base_url('assets/js/active.js') ?>"></script>
    <!-- Live Chat Code :: Start of Tawk.to Script -->
    <script>
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            // s1.src = 'https://embed.tawk.to/5b55ea72df040c3e9e0bdf85/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!-- End of Tawk.to Script -->
</body>

</html>