<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>E-POKIR</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url('assets/img/core-img/favicon.ico') ?>">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('test/style.css') ?>">
    <style>
      .single-service-area .course-icon {
        background-color: #576d3f;
      }

#more {display: none;}

            .content
            {
                position: fixed;
                top: 0;
                bottom: 0;
                background: rgba(0.5, 0, 0, 0.7);
                color: black;
                width: 100%;
                padding: 0;
            }
</style>

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Navbar Area -->
        <div class="fitness-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="fitnessNav">

                        <!-- Nav brand -->
                        <a href="<?php echo site_url('masyarakat/overview/index') ?>" class="nav-brand"><img width="350px" height="100px" src="<?php echo base_url('assets/img/core-img/b.png') ?>" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li class ="nav-tabs" style="font-size: 600px"><a href="<?php echo site_url('masyarakat/overview/index') ?>">Beranda</a></li>
                                 
                                   <!-- <li><a href="<?php echo site_url('masyarakat/overview/index') ?>#adalah">Tentang</a></li> -->
                                    <li ><a href="<?php echo site_url('masyarakat/overview/about') ?>">DPRD</a></li>
                                    <li><a href="<?php echo site_url('masyarakat/overview/aspirasi/') ?>">Usulan</a></li>
                                  <!--   <li><a href="contact.html">Contact</a></li> -->
                                </ul>

                                <!-- Call Button -->
                                <a href="<?php echo site_url('auth') ?>" align="center" class="fitness-btn menu-btn ml-30">Masuk</a>

                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
   
    <section class="hero-area">
        <div class="hero-slides owl-carousel">

            
            <?php $i = 1; ?>
            <?php foreach($aspirasi as $row) { ?>
            <?php $gambar = base_url() . "assets/img/bg-img/Pictures/log" . $i . ".jpg"; ?>
            <!-- Single Hero Slide -->
             <div class="single-hero-slide bg-img" style="background-image: url('<?php echo $gambar; ?>');">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 col-lg-9">
                            <div class="hero-slides-content">
                               <h2 data-animation="fadeInUp" data-delay="100ms">Usulan Terbaru</h2>
                                <p data-animation="fadeInUp" data-delay="400ms"><?php echo $row->usulan . " sebanyak " . $row->volume . " di " . $row->lokasi . " ..."; ?></p>
                    <a class="navbar-brand page-scroll">
                               <a  href="<?php echo site_url('masyarakat/overview/aspirasi/') ?>" class="btn fitness-btn wel-btn" data-animation="fadeInUp" data-delay="700ms">Selengkapnya</a> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>

    </section>
  
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Fitness Adds Area End ##### -->

     <div id="adalah" class="fitness-services-area section-padding-100-0">
       <div class="container">
           <div class="row">
    


               <div class="col-12 col-sm-6 col-lg-4">
                   <div class="single-service-area mb-100">
                       <div class="course-content d-flex align-items-center">
                           <!-- Course Icon -->
                             <div class="single-service-area">
                                    <img src="<?php echo base_url('assets/img/core-img/1.png'); ?>" width="90px" alt="">
                                    
                                </div>
                              &nbsp;&nbsp;&nbsp;&nbsp;
                           <h4>Tentang</h4>
                       </div>
                       <font size="4px">
                       <p align="justify">Pokok pikiran DPRD memuat pandangan dan pertimbangan dari hasil reses DPRD, mengenai arah prioritas pembangunan serta rumusan usulan kebutuhan Program/Kegiatan yang bersumber dari hasil <span id="dots">...</span><span id="more">penelaahan pokok pikiran DPRD. Menyikapi kemungkinan penyimpangan dalam perencanaan dan penganggaran APBD, Pemerintah Kota Malang dan DPRD Kota Malang berkomitmen meminimalisirnya melalui aplikasi e-POKIR (Pokok-pokok pikiran - elektronik). E-POKIR merupakan aplikasi yang akan diisi (diinput) masing-masing anggota legislatif sebagai penyampaian aspirasi dari masyarakat secara online untuk kemudian ditindaklanjuti Badan anggaran (bangar) untuk di ajukan kepada eksekutif dalam perencanaan APBD. </span></font>
                      <a  href="#" onclick="myFunction()"class=""  data-delay="700ms" id="myBtn"> <font size="4px">Selengkapnya </font></a></p>
                    
                   </div>
               </div>
   
               <!-- Single Service Area -->
               <div class="col-12 col-sm-6 col-lg-4">
                   <div class="single-service-area mb-100">
                       <div class="course-content d-flex align-items-center">
                           <!-- Course Icon -->
                          <div class="single-service-area">
                                    <img src="<?php echo base_url('assets/img/core-img/2.png'); ?>" width="90px" alt="">
                                    
                                </div>
                                 &nbsp;&nbsp;&nbsp;&nbsp;
                                                          <h4>Manfaat</h4>
                                                      </div><font size="4px">
                                                      <p align="justify">
                                                       • Mempermudah anggota DPRD dalam menyampaikan aspirasi masyarakat ke anggota APBD. <br>
                                                       • Mempermudah masyarakat mengetahui aspirasi yang telah disetujui oleh anggota APBD</font></p>
                                                  </div>
                                              </div>
                                  
                                              <!-- Single Service Area -->
                                              <div class="col-12 col-sm-6 col-lg-4">
                                                  <div class="single-service-area mb-100">
                                                      <div class="course-content d-flex align-items-center">
                                                          <!-- Course Icon -->
                                                                <div class="single-service-area">
                                    <img src="<?php echo base_url('assets/img/core-img/3.png'); ?>" width="90px" alt="">
                                    
                                </div>
                                 &nbsp;&nbsp;&nbsp;&nbsp;
                           <h4>Tujuan</h4>
                       </div><font size="4px">
                       <p align="justify">
                        • Membangun sistem e-POKIR yang dapat mempermudah anggota DPRD dalam penyampaian aspirasi masyarakat. <br>
                        • Meminimalisir penyimpangan dalam perancangan dan penganggaran APBD.</font></p>
                   </div>
               </div>
           </div>
       </div>
   </div>




    <!-- ##### About Us Area Start ##### -->
    <!-- <div id="adalah" class=" section-wrapper about-us-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <CENTER>
                        <h1>E-POKIR</h1>
                        <h4>Pokok Pikiran Elektronik Kota Malang</h4>
                           <p>Pokok pikiran DPRD memuat pandangan dan pertimbangan dari hasil reses DPRD, mengenai arah prioritas pembangunan serta rumusan usulan kebutuhan Program/Kegiatan yang bersumber dari hasil penelaahan pokok pikiran DPRD. Menyikapi kemungkinan penyimpangan dalam perencanaan dan penganggaran APBD, Pemerintah Kota Malang dan DPRD Kota Malang berkomitmen meminimalisirnya melalui aplikasi e-POKIR (Pokok-pokok pikiran - elektronik). E-POKIR merupakan aplikasi yang akan diisi (diinput) masing-masing anggota legislatif sebagai penyampaian aspirasi dari masyarakat secara online untuk kemudian ditindaklanjuti Badan anggaran (bangar) untuk di ajukan kepada eksekutif dalam perencanaan APBD. </p>
                    </div>
                </div>
            </div> -->
         


            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="row">
                        <!-- About Text -->
                        <div class="col-12 col-lg-6">
                            <div class="about-text">
                              
                               
                            </div>
                        </div>
                    </CENTER>
                    <br>
                        <!-- About Text -->
                       <!--  <div class="col-12 col-lg-6">
                           <div class="about-text">
                               <p>Proin nunc sapien, gravida ut sapien ut, ultrices faucibus sapien. Proin vehicula varius ex, vel feugiat massa scelerisque id. Nullam vulputate a lectus non molestie. Duis at lobortis neque, in maximus leo. Donec nec blandit ex, in finibus tortor. Curabitur aliquet fermentum ultrices.</p>
                           </div>
                       </div> -->
                        <!-- About Button Group -->
                        <!-- <div class="col-12">
                            <div class="about-btn-group mt-50">
                                <a href="#" class="btn fitness-btn mr-3">Get a membership</a>
                                <a href="#" class="btn fitness-btn btn-2">Find out more</a>
                            </div>
                        </div>
                                            </div>
                                        </div> -->
                <div class="col-12 col-lg-4">
                    <div class="about-thumb">
                        <!-- <img src="img/bg-img/man.png" alt=""> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Experts Area Start ##### -->
   

                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="fitness-services-area section-padding-100-0">
     
               </div>
           </div>
       </div>
   </div>



    <!-- ##### Experts Area End ##### -->


    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area section-padding-100-0 bg-img bg-overlay" style="background-image: url('<?php echo base_url('assets/img/bg-img/log2.jpg'); ?>');">
    
        <div class="main-footer-area">
            <div class="container">
                <div class="row">
    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget-area mb-50">
                            <!-- Widget Title -->
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url('assets/img/core-img/hmm.png') ?>" alt=""></a>
                            </div>
                            <p class="mb-30">Pokok pikiran DPRD memuat pandangan dan pertimbangan dari hasil reses DPRD, mengenai arah prioritas pembangunan serta rumusan usulan kebutuhan Program/Kegiatan yang bersumber dari hasil penelaahan pokok pikiran DPRD</p>
                            
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
    

                   <!--  Footer Widget Area -->
                    
                     
                   <!--  Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget-area mb-50">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                    
                            <!-- Single Contact -->
                            <div class="single-contact mb-30">
                                <span>Alamat</span>
                                <p>Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119 </p>
                            </div>
                    
                            <!-- Single Contact -->
                            <div class="single-contact mb-30">
                                <span>Telepon</span>
                                <p> (0341) 325617</p>
                            </div>
                    
                            <!-- Single Contact -->
                            <div class="single-contact mb-30">
                                <span>Email</span>
                                <p>epokirmalang@gmail.com</p>
                            </div>
    
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-6">
                        <div class="footer-widget-area mb-50">

                            <!-- Widget Title -->
                            <div class="widget-title">
                                <h6>Peta Lokasi</h6>
                            </div>

                            <div class="col-12 col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d31608.48946402963!2d112.6091218!3d-7.9926189!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x55d58562ceec5085!2sDPRD+Malang+City!5e0!3m2!1sen!2sid!4v1547727950365" width="350" height="310" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        </div>
    
    
                </div>
            </div>
        </div>
    
       <!--  Copywrite Area -->
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p><a href="#"> Copyright  &copy;<script>document.write(new Date().getFullYear());</script> e-POKIR  Kota Malang <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">NCC Kota Malang</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url('assets/js/jquery/jquery-2.2.4.min.js') ?>"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url('assets/js/bootstrap/popper.min.js') ?>"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url('assets/js/plugins/plugins.js') ?>"></script>
     
    <script src="<?php echo base_url('assets/js/jquery.nav.js') ?>"></script>
    <!-- Active js -->
    <script src="<?php echo base_url('assets/js/active.js') ?>"></script>

    <script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Selengkapnya"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Lebih sedikit"; 
    moreText.style.display = "inline";
  }
}
</script>
    <!-- Live Chat Code :: Start of Tawk.to Script -->
    <script>
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
           /* s1.src = 'https://embed.tawk.to/5b55ea72df040c3e9e0bdf85/default';
*/            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!-- End of Tawk.to Script -->
</body>

</html>