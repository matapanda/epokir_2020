<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('dprd/c_dprd') ?>">
            <i class="fas fa-fw fa-home"></i>
            <span>Beranda</span>
        </a>
    </li>
    <!-- <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-city"></i>
            <span>Manajemen Dapil</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil/add') ?>">New Dapil</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil') ?>">List Dapil</a>
        </div>
    </li> -->

     <!-- <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Manajemen DPRD</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil/add') ?>">New DPRD</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil') ?>">List DPRD</a>
        </div>
    </li> -->

    <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-list"></i>
            <span>Manajemen Aspirasi</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('dprd/aspirasi/add') ?>">Tambah Aspirasi</a>
            <a class="dropdown-item" href="<?php echo site_url('dprd/aspirasi') ?>">Daftar Aspirasi</a>
            <a class="dropdown-item" href="<?php echo site_url('dprd/aspirasi/get_arsip_report') ?>">Report</a> 
        </div>
    </li>

     <li class="nav-item <?php echo $this->uri->segment(2) == 'bantuan' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('dprd/bantuan') ?>">
            <i class="fas fa-fw fa-question"></i>
            <span>Tata Cara Pengisian Aspirasi</span>
        </a>
    </li>

    <!-- <li class="nav-item dropdown <?php echo $this->uri->segment(2) == 'dapil' ? 'active': '' ?>">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Manajemen User</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil/add') ?>">New User</a>
            <a class="dropdown-item" href="<?php echo site_url('admin/dapil') ?>">List User</a>
        </div>
    </li> -->


    <!-- <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li> -->
</ul>