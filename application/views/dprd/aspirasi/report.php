<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("dprd/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("dprd/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("dprd/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("dprd/_partials/breadcrumb.php") ?>


					<div class="col-md-4">
					<!-- <form method="post" enctype="multipart/form-data" action="user.php"> -->
						
						<!-- <div class="card-header"> -->

								<a href="<?php echo site_url('dprd/aspirasi/add') ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Tambah Baru</a>

        				
</div>
<br>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-5">
										<label for="volume">Tanggal Start*</label>
										<input type="date" class="form-control" id="tgl_start" name="tgl_start" value="<?php print_r(date("Y-m-d"))?>" />
									</div>
									<div class="col-md-5">
										<label for="volume">Tanggal Finish*</label>
										<input type="date" class="form-control" id="tgl_finish" name="tgl_finish" value="<?php print_r(date("Y-m-d"))?>"/>
									</div>
									<div class="col-md-1 text-right">
										<label for="volume">&nbsp;</label><br>
										<button class="btn btn-success" id="btn_filter" name="btn_filter">Filter</button>
									</div>
									<div class="col-md-1 text-right">
										<label for="volume">&nbsp;</label><br>
										<button class="btn btn-success" id="btn_print" name="btn_print">Print</button>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="table-responsive"><br><br>
									<table class="table table-bordered table-striped" id="dataTable" >
										<thead>
											<tr align="center">
												<th>No</th>
							                    <th>Usulan</th>
							                    <th>Volume</th>
							                    <th>Lokasi</th>
							                    <th>Tanggal Usulan</th>
							                    <th>Perangkat Daerah</th>
							                    <th>Anggota DPRD</th>
							                    <th>Dapil</th>
							                    <th>Foto Aspirasi</th>
							                    <th>Keterangan</th>
							                  <!--   <th>Status</th> -->
												<th style="text-align: center;">Status</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1; 
											foreach ($aspirasi as $key => $aspirasi){
												$str_btn = "";
												 

												if($aspirasi->status == 'Belum Dikonfirmasi'){
													$str_btn = "<a href=\"".site_url('dprd/aspirasi/edit/'.$aspirasi->id)."\" class=\"btn btn-edit\"><i class=\"fas fa-edit\"></i> Edit</a>

														<a onclick=\"deleteConfirm('".site_url('dprd/aspirasi/delete/'.$aspirasi->id)."')\" href=\"#!\" class=\"btn btn-delete text-danger\"><i class=\"fas fa-trash\"></i> Hapus</a>";
												} else{
													$sts = $aspirasi->status;
													if($sts=='Diterima'){
						                            	$str_btn = "<button class=\"btn btn-success\" clas=\"btn-xs\">Diterima</button>";
						                            }else if($sts=='Ditolak'){
						                            	$str_btn = "<button class=\"btn btn-danger btn-xs\">&nbsp;Ditolak&nbsp;&nbsp;</button>";    
						                            }else{
						                            	$str_btn = "";
						                            }
												}
												print_r("<tr>
															<td>".$i."</td>
															<td style=\"min-width:177px;\">".$aspirasi->usulan."</td>
															<td>".$aspirasi->volume." ".$aspirasi->nama_satuan."</td>
															<td>".$aspirasi->lokasi."</td>
															<td>".date('d-m-Y', strtotime($aspirasi->tanggal))."</td>
															<td>".$aspirasi->nama_perangkat."</td>
															<td>".$aspirasi->nama_dprd."</td>
															<td>".$aspirasi->nama_dapil."</td>
															<td><img width=\"200px\" height=\"100px\" src=\"".base_url()."upload/aspirasi/".$aspirasi->foto_as."\"></td>
															<td>".$aspirasi->keterangan."</td>
															<td style=\"min-width:95px;\" align=\"center\" width=\"250\">".$str_btn."</td>
														</tr>");
												$i++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>	
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("dprd/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
	<?php $this->load->view("dprd/_partials/modal.php") ?>

	<?php $this->load->view("dprd/_partials/js.php") ?>
	
	<?php 
		$tgl_start 	= $_GET["tgl_start"];
		$tgl_finish = $_GET["tgl_finish"];
	?>
	<script src="<?php print_r(base_url());?>js/jquery-3.2.1.js"></script>
	<script>
		$(document).ready(function(){
			set_default();
		});

		function set_default(){
			$("#tgl_start").val("<?php print_r($tgl_start);?>");
			$("#tgl_finish").val("<?php print_r($tgl_finish);?>");
		}

		function deleteConfirm(url){
			console.log(url);
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}

		$("#btn_filter").click(function(){
			var tgl_start = $("#tgl_start").val();
			var tgl_finish = $("#tgl_finish").val();

			window.location.href = "<?php print_r(base_url())?>dprd/aspirasi/get_arsip_report?tgl_start="+tgl_start+"&tgl_finish="+tgl_finish+"&id=35";
		});

		$("#btn_print").click(function(){
			var tgl_start = $("#tgl_start").val();
			var tgl_finish = $("#tgl_finish").val();

			window.location.href = "<?php print_r(base_url())?>dprd/aspirasi/cetak?tgl_start="+tgl_start+"&tgl_finish="+tgl_finish+"&id=35";

			// window.location.href = "<?php print_r(base_url())?>dprd/aspirasi/cetak";
		});
	</script>

</body>

</html>