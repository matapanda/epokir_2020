<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("dprd/_partials/head.php") ?>
</head>

<body id="page-top"> 

	<?php $this->load->view("dprd/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("dprd/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("dprd/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<!-- Card  -->
				<div class="card mb-3">
					<div class="card-header">

						<a href="<?php echo site_url('dprd/aspirasi/') ?>"><i class="fas fa-arrow-left"></i>
							Kembali</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('dprd/aspirasi/edit') ?>" method="post" enctype="multipart/form-data">

							<input type="hidden" name="id" value="<?php echo $aspirasi->id?>" />



							<div class="form-group">
								<label for="usulan">Usulan*</label>
								<input class="form-control <?php echo form_error('usulan') ? 'is-invalid':'' ?>"
								 type="text" name="usulan" placeholder="Inputkan Usulan" value="<?php echo $aspirasi->usulan ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('usulan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="volume">Volume</label>
								<input class="form-control <?php echo form_error('volume') ? 'is-invalid':'' ?>"
								 name="volume" placeholder="Inputkan volume" value="<?php echo $aspirasi->volume ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('volume') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id">Satuan</label>
								<select name="id_satuan" class="form-control" required="required">
									<option value="" selected="selected">Pilih Satuan</option>
									<?php foreach($satuan as $data) { ?>
									<option value="<?php echo $data['id']; ?>" <?php echo ($aspirasi->id_satuan == $data['id'] ? 'selected="selected"' : ''); ?>><?php echo $data['nama_satuan']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="lokasi">Lokasi*</label>
								<input class="form-control <?php echo form_error('lokasi') ? 'is-invalid':'' ?>"
								 type="text" name="lokasi"  placeholder="Inputkan Alamat Tempat yang Diusulkan" value="<?php echo $aspirasi->lokasi ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('lokasi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal">Tanggal*</label>
								<input class="form-control <?php echo form_error('tanggal') ? 'is-invalid':'' ?>"
								 type="text" name="tanggal"  placeholder="Tanggal" readonly="readonly" value="<?php echo date('Y-m-d'); ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('tanggal') ?>
								</div>
							</div>	

							<div class="form-group">
								<label for="id_perangkat_daerah">Perangkat Daerah*</label>
								<select name="id_perangkat_daerah" class="form-control" required="required">
									<option value="" selected="selected">Pilih Perangkat Daerah</option>
									<?php foreach($perangkat_daerah as $data) { ?>
									<option value="<?php echo $data['id']; ?>" <?php echo ($aspirasi->id_perangkat_daerah == $data['id'] ? 'selected="selected"' : ''); ?>><?php echo $data['nama_perangkat']; ?></option>
									<?php } ?>
								</select>
							</div>

							<!-- load nya salah -->
							<div class="form-group">
								<label for="id_anggota_dprd">Anggota DPRD*</label>
								<select name="id_anggota_dprd" class="form-control" required="required">
									<option value="" selected="selected">Pilih Anggota DPRD</option>
									<?php foreach($dprd as $data) { ?>
									<option value="<?php echo $data['id']; ?>" <?php echo ($aspirasi->id_anggota_dprd == $data['id'] ? 'selected="selected"' : ''); ?>><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="iddapil">Dapil*</label>
								<select name="id_dapil" class="form-control" required="required">
									<option value="" selected="selected">Pilih Dapil</option>
									<?php foreach($dapil as $data) { ?>
									<option value="<?php echo $data['id']; ?>" <?php echo ($aspirasi->id_dapil == $data['id'] ? 'selected="selected"' : ''); ?>><?php echo $data['nama']; ?></option>
									<?php } ?>
								</select>
							</div>
									<div class="form-group">
								<label for="foto">Foto Aspirasi</label>
								<br>
								<img algn="center" style="width: 190px; height: 240px;" src="<?php echo base_url('upload/aspirasi/'.$aspirasi->foto_as) ?>"/>
								<br>
								<br>
								<input type="file" name="foto_as" id="foto_as" class="form-control"  />
								 <input  name="old_image" value="<?php echo $aspirasi->foto_as; ?>" />
							
							</div>
							<div class="form-group">
								<label for="keterangan">Keterangan</label>
								<input class="form-control <?php echo form_error('keterangan') ? 'is-invalid':'' ?>"
								 name="keterangan" placeholder="Inputkan Keterangan" value="<?php echo $aspirasi->keterangan ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('keterangan') ?>
								</div>
							</div>
							
							<input class="btn btn-success" type="submit" name="btn" value="Simpan" />
						</form>

					</div>

					<div class="card-footer small text-muted">
							* wajib diisi 
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("dprd/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
		<?php $this->load->view("dprd/_partials/modal.php") ?>
		<?php $this->load->view("dprd/_partials/js.php") ?>

</body>

</html>