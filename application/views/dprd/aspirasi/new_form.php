<?php if($this->session->userdata('logged_in') != "Sudah Login"){
      redirect(base_url("auth"));
    }?>
 <!--    <?php if($this->session->userdata('level') != "dprd"){ 
    	redirect(base_url("admin"));
    }?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("dprd/_partials/head.php") ?>
</head>

<body id="page-top">


	<?php $this->load->view("dprd/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("dprd/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("dprd/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('dprd/aspirasi/') ?>"><i class="fas fa-arrow-left"></i>Kembali</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('dprd/aspirasi/add') ?>" method="post" enctype="multipart/form-data" >
							
							<!-- <div class="form-group">
								<label for="name">ID </label>
								<input class="form-control <?php echo form_error('id') ? 'is-invalid':'' ?>"
								 type="number" name="id"   />
								<div class="invalid-feedback">
									<?php echo form_error('id') ?>
								</div>
							</div> -->

							<div class="form-group">
								<label for="usulan">Usulan*</label>
								<input class="form-control <?php echo form_error('usulan') ? 'is-invalid':'' ?>"
								 name="usulan" placeholder="Masukkan Usulan"></input>
								<div class="invalid-feedback">
									<?php echo form_error('usulan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="volume">Volume*</label>
								<input class="form-control <?php echo form_error('volume') ? 'is-invalid':'' ?>"
								 name="volume" placeholder="Masukkan volume"></input>
								<div class="invalid-feedback">
									<?php echo form_error('volume') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="id">Satuan*</label>
								<select name="id_satuan" class="form-control" required="required">
									<option value="" selected="selected">Pilih Satuan</option>
									<?php foreach($satuan as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama_satuan']; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group">
								<label for="lokasi">Lokasi*</label>
								<input class="form-control <?php echo form_error('lokasi') ? 'is-invalid':'' ?>"
								 type="text" name="lokasi"  placeholder="Masukkan Alamat Tempat yang Diusulkan" />
								<div class="invalid-feedback">
									<?php echo form_error('lokasi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal">Tanggal*</label>
								<input class="form-control <?php echo form_error('tanggal') ? 'is-invalid':'' ?>"
								 type="text" name="tanggal"  placeholder="Tanggal" readonly="readonly" value="<?php echo date('d-m-Y'); ?>" />
								<div class="invalid-feedback">
									<?php echo form_error('tanggal') ?>
								</div>
							</div>	

							<div class="form-group">
								<label for="id_perangkat_daerah">Perangkat Daerah*</label>
								<select name="id_perangkat_daerah" class="form-control" required="required">
									<option value="" selected="selected">Pilih Perangkat Daerah</option>
									<?php foreach($perangkat_daerah as $data) { ?>
									<option value="<?php echo $data['id']; ?>"><?php echo $data['nama_perangkat']; ?></option>
									<?php } ?>
								</select>
							</div>						

							<?php
								$id_anggota_dprd = "";
								$nama_anggota_dprd = "";

								foreach($dprd as $data) {
									if($data['id'] == $this->session->userdata('id_dprd')) {
										$id_anggota_dprd = $data['id'];
										$nama_anggota_dprd = $data['nama'];
									}
								}
							?>
							<div class="form-group">
								<label for="id_anggota_dprd">Anggota DPRD*</label>
								<input class="form-control"
								 type="hidden" name="id_anggota_dprd" readonly="readonly" value="<?php echo $id_anggota_dprd; ?>" />
								<input class="form-control"
								 type="text" readonly="readonly" value="<?php echo $nama_anggota_dprd; ?>" />
							</div>

							<?php
								$id_dapil = "";
								$nama_dapil = "";

								foreach($dapil as $data) {
									if($data['id'] == $this->session->userdata('id_dapil')) {
										$id_dapil = $data['id'];
										$nama_dapil = $data['nama'];
									}
								}
							?>
							<div class="form-group">
								<label for="id_dapil">Dapil*</label>
								<input class="form-control"
								 type="hidden" name="id_dapil" readonly="readonly" value="<?php echo $id_dapil; ?>" />
								<input class="form-control"
								 type="text" readonly="readonly" value="<?php echo $nama_dapil; ?>" />
							</div>

							<div class="form-group">
								<label for="keterangan">Foto</label>
								<input type="file" class="form-control" id="foto_as" name="foto_as" placeholder="Masukkan Keterangan"></input>
								<div class="invalid-feedback">
									<?php echo form_error('keterangan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="keterangan">Keterangan</label>
								<input class="form-control <?php echo form_error('keterangan') ? 'is-invalid':'' ?>"
								 name="keterangan" placeholder="Masukkan Keterangan"></input>
								<div class="invalid-feedback">
									<?php echo form_error('keterangan') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Simpan" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* wajib diisi 
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("dprd/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
		<?php $this->load->view("dprd/_partials/modal.php") ?>
		<?php $this->load->view("dprd/_partials/js.php") ?>

</body>

</html>