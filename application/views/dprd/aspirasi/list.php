<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("dprd/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("dprd/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("dprd/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("dprd/_partials/breadcrumb.php") ?>


					<div class="col-md-4">
					<form method="post" enctype="multipart/form-data" action="user.php">
						
						<!-- <div class="card-header"> -->

								<a href="<?php echo site_url('dprd/aspirasi/add') ?>""  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Tambah Baru</a>

        			<!-- 	<a href="<?php echo base_url('admin/dapil/export'); ?>"  class="btn btn-success" style="text-decoration: none" ><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a> -->

        				
</div>
<br>

				<!-- DataTables -->
				<div class="card mb-3">
					<!-- <div class="card-header">
						<a href="<?php echo site_url('dprd/aspirasi/add') ?>"><i class="fas fa-plus"></i> Tambah Baru</a>
					</div>
						<form method="post" enctype="multipart/form-data" action="dprd.php">
									<div class="card mb-3">
					<div class="card-header">
					        				<a href="<?php echo base_url('dprd/aspirasi/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>
					        				<a href="<?php echo base_url('admin/dprd/import'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> importtt Data Excel</a>
					        				<button class="form-control btn btn-default" data-toggle="modal" data-target="#import-dprd"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button>
					    			</div>
					 -->
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" >
								<thead>
									<tr align="center">
										<th>No</th>
					                    <th>Usulan</th>
					                    <th>Volume</th>
					                    <th>Lokasi</th>
					                    <th>Tanggal Usulan</th>
					                    <th>Perangkat Daerah</th>
					                    <th>Anggota DPRD</th>
					                    <th>Dapil</th>
					                    <th>Foto Aspirasi</th>
					                    <th>Keterangan</th>
					                  <!--   <th>Status</th> -->
										<th style="text-align: center;">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=0; ?>
									<?php foreach ($aspirasi as $aspirasi): ?>
									<tr>
										<td><?php echo ++$i; ?></td>
										<td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
										<td><?php echo $aspirasi->volume." ".$aspirasi->nama_satuan ?></td>
										
										<td><?php echo $aspirasi->lokasi ?></td>
										<td><?php echo date('d-m-Y', strtotime($aspirasi->tanggal)); ?></td>
										<td><?php echo $aspirasi->nama_perangkat ?></td>
										<td><?php echo $aspirasi->nama_dprd ?></td>
										<td><?php echo $aspirasi->nama_dapil ?></td>
										<td><?php echo "<img width=\"200px\" height=\"100px\" src=\"".base_url()."upload/aspirasi/".$aspirasi->foto_as."\">"; ?></td>
										<td><?php echo $aspirasi->keterangan ?></td>
										<!-- <td><?php echo $aspirasi->status ?></td> -->
										<td style="min-width:95px;" align="center" width="250">
											<?php if($aspirasi->status == 'Belum Dikonfirmasi') { ?>
											<a href="<?php echo site_url('dprd/aspirasi/edit/'.$aspirasi->id) ?>"
											 class="btn btn-edit"><i class="fas fa-edit"></i> Edit</a>

											<a onclick="deleteConfirm('<?php echo site_url('dprd/aspirasi/delete/'.$aspirasi->id) ?>')"
											 href="#!" class="btn btn-delete text-danger"><i class="fas fa-trash"></i> Hapus</a>
											<?php } else { ?>
												<div class="label label-primary">
													<?php 
													$sts = $aspirasi->status; ?></div>


							<?php
                            if($sts=='Diterima'){
                            echo "<button class=\"btn btn-success\" clas=\"btn-xs\">Diterima</button>";
                            }else if($sts=='Ditolak'){
                            echo "<button class=\"btn btn-danger btn-xs\">&nbsp;Ditolak&nbsp;&nbsp;</button>";    
                            }else{
                              echo "";
                            }
                            ?>

											<?php } ?>
										</td> 
										
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("dprd/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
	<?php $this->load->view("dprd/_partials/modal.php") ?>

	<?php $this->load->view("dprd/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			console.log(url);
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

</body>

</html>