<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("dprd/_partials/head.php") ?>
</head>
<body id="page-top">

<?php $this->load->view("dprd/_partials/navbar.php") ?>

<div id="wrapper">

  <?php $this->load->view("dprd/_partials/sidebar.php") ?>

  <div id="content-wrapper">

    <div class="container-fluid">

        <!-- 
        karena i//ni halaman overview (home), kita matikan partial breadcrumb.
        Jika anda ingin mengampilkan breadcrumb di halaman overview,
        silahkan hilangkan komentar (//) di tag PHP di bawah.
        -->
    <?php //$this->load->view("admin/_partials/breadcrumb.php") ?>

    <!-- Icon Cards-->
    <div align="center">
    <img src="<?php echo base_url('assets/img/core-img/b.png') ?>" width="400px">
    </div>
      <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-home"></i>
            Beranda</div>
          <div class="card-body">
            <p>Selamat Datang  <?php echo $this->session->userdata("id");?> 

            <br>Halaman ini digunakan untuk memasukan aspirasi dan akan ditanggapi oleh Barenlitbang</p>
             <div class="row">
      
      <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-primary o-hidden h-100">
        <div class="card-body">
        <div class="card-body-icon">
          <i class="fas fa-fw fa-list"></i>
        </div>
        <div class="mr-5">Aspirasi</div>
        </div>
        <a class="card-footer text-white clearfix small z-1" href="aspirasi">
        <span class="float-left">Lihat Detail</span>
        <span class="float-right">
          <i class="fas fa-angle-right"></i>
        </span>
        </a>
      </div>
      </div>
    </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>
  
        <div class="row">
        <div class="col-lg-6">
         <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-list"></i>
            Aspirasi Terbaru</div>
          <div class="card-body">
            <div class="col-lg-6">
   
      </div>
      <br>
              <div class="table-responsive">
              <table class="table table-bordered table-striped" id="dataTable" >
                <thead>
                  <tr align="center">
                    <th>No</th>
                              <th>Usulan</th>
                            
                              <th>Anggota DPRD</th>
                              
                    <!-- <th style="text-align: center;">Aksi</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; ?>
                  <?php foreach ($aspirasi2 as $aspirasi2): ?>
                  <tr>
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:177px;"><?php echo $aspirasi2->usulan ?></td>
                  
                    <td><?php echo $aspirasi2->nama_dprd ?></td>
                  
                    <!-- <td width="250" align="center">
                      <a href="<?php echo site_url('admin/aspirasi/edit/'.$aspirasi->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/aspirasi/delete/'.$aspirasi->id) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td> -->
                    
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>

          </div>
          <div class="card-footer small text-muted"></div>
        </div>
    </div>

    <div class="col-lg-6">
         <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-list"></i>
            Aspirasi Anda</div>
          <div class="card-body">
          
      <br>
               <div class="table-responsive">
              <table class="table table-bordered table-striped" id="dataTable" >
                <thead>
                  <tr align="center">
                    <th>No</th>
                              <th>Usulan</th>
                            
                              <th>Status</th>
                              
                    <!-- <th style="text-align: center;">Aksi</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; ?>
                  <?php foreach ($aspirasi as $aspirasi): ?>
                  <tr>
                    <td><?php echo ++$i; ?></td>
                    <td style="min-width:177px;"><?php echo $aspirasi->usulan ?></td>
                  
                   <td style="min-width:95px;" align="center" width="250">
                      <?php if($aspirasi->status == 'Belum Dikonfirmasi') { ?>
                      <a href="<?php echo site_url('dprd/aspirasi/edit/'.$aspirasi->id) ?>"
                       class="btn btn-edit"><i class="fas fa-edit"></i> Edit</a>

                      <a onclick="deleteConfirm('<?php echo site_url('dprd/aspirasi/delete/'.$aspirasi->id) ?>')"
                       href="#" class="btn btn-delete text-danger"><i class="fas fa-trash"></i> Hapus</a>
                      <?php } else { ?>
                        <div class="label label-primary">
                          <?php 
                          $sts = $aspirasi->status; ?></div>


              <?php
                            if($sts=='Diterima'){
                            echo "<button class=\"btn btn-success\" clas=\"btn-xs\">Diterima</button>";
                            }else if($sts=='Ditolak'){
                            echo "<button class=\"btn btn-danger btn-xs\">&nbsp;Ditolak&nbsp;&nbsp;</button>";    
                            }else{
                              echo "";
                            }
                            ?>

                      <?php } ?>
                    </td> 
                    <!-- <td width="250" align="center">
                      <a href="<?php echo site_url('admin/aspirasi/edit/'.$aspirasi->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a onclick="deleteConfirm('<?php echo site_url('admin/aspirasi/delete/'.$aspirasi->id) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                    </td> -->
                    
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>


          </div>
          <div class="card-footer small text-muted"></div>
        </div>
    </div>
    </div>
  <!--   <div id="content-wrapper">
      <div class="container-fluid">
        <img src="<?=base_url()?>assets/b.jpg" width="1300px" height="550px">
      
      </div>
    </div> -->

    <!-- Area Chart Example-->
    <!-- <div class="card mb-3">
      <div class="card-header">
      <i class="fas fa-chart-area"></i>
      Visitor Stats</div>
      <div class="card-body">
      <canvas id="myAreaChart" width="100%" height="30"></canvas>
      </div>
      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div> -->

    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
    <?php $this->load->view("admin/_partials/footer.php") ?>

  </div>
<!-- /#wrapper -->

<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
<?php $this->load->view("dprd/_partials/modal.php") ?>
<?php $this->load->view("dprd/_partials/js.php") ?>

<script>
    function deleteConfirm(url){
      console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    } 
  </script>

    
</body>
</html>