<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("dprd/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("dprd/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("dprd/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("dprd/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					
					<div class="card-body">

		<div class="jumbotron">
			<center><a><img src="<?php echo base_url('assets/img/bantuan/1.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Klik pada menu <b>"Manajemen Aspirasi"</b> kemudian pilih <b>"Tambah Aspirasi"</b></p></center>
			
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/2.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Masukkan <b>"Usulan"</b> Anda </p></center>
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/3.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Masukkan <b>"Volume(jumlah)"</b> yang sesuai dengan usulan Anda  </p></center>
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/4.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Pilih <b>"Satuan"</b> yang sesuai dengan Volume yang dimasukkan </p></center>
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/5.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Masukkan <b>"Lokasi"</b> sesuai tempat usualan dengan lengkap </p></center>	
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/6.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Pilih <b>"Perangkat Daerah"</b> yang sesuai dengan bidangnya </p></center>		
		<br>
			<center><a><img src="<?php echo base_url('assets/img/bantuan/7.JPG') ?>" alt=""></a>
				<br>
				<br>
				<p>Masukkan <b>"Keterangan"</b> jika ada keterangan yang ingin dimasukkan</p></center>	
		<br>
			<center>
				<p>Pastikan terisi semua pada kolom diatas, jika terisi semua Anda bisa klik <b>"Simpan".</b>  Anda bisa memasukkan beberapa usulan dengan .......................... </p></center>	
		</div>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<?php $this->load->view("dprd/_partials/footer.php") ?>
	<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
	<?php $this->load->view("dprd/_partials/modal.php") ?>

	<?php $this->load->view("dprd/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

</body>

</html>