<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("dprd/_partials/head.php") ?>
</head>
<body id="page-top">

<?php $this->load->view("dprd/_partials/navbar.php") ?>

<div id="wrapper">

  <?php $this->load->view("dprd/_partials/sidebar.php") ?>

  <div id="content-wrapper">

    <div class="container-fluid">

        <!-- 
        karena i//ni halaman overview (home), kita matikan partial breadcrumb.
        Jika anda ingin mengampilkan breadcrumb di halaman overview,
        silahkan hilangkan komentar (//) di tag PHP di bawah.
        -->
    <?php //$this->load->view("admin/_partials/breadcrumb.php") ?>

    <!-- Icon Cards-->
<!--     <div align="center">
    <img src="<?php echo base_url('assets/img/core-img/b.png') ?>" width="400px">
    </div> -->

  
        <div class="row">
        <div class="col-lg-12">
         <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-user"></i>
            Profile Anggota DPRD  </div>
          <div class="card-body">
            <div class="col-lg-12">
                 <div id="printTable">
              
                    <table border="0" width="auto">
   <?php foreach($profile_id as $data){ ?>
<center><h2><u>DETAIL ANGGOTA DPRD</u></h2>
<br>
<img width='300' height='220' src="<?php echo base_url()."upload/product/".$data->foto_dprd; ?>" alt="" class="img-responsive" style="object-fit: contain">
</center>
<tr>
    <td></td>
    <td width="40%"><p style ="margin-left:170px"></p></td>
</tr>

<tr>
    <td></td>
    <td> Nama Anggota </td> 
    <td>:</td> 
    <td><?=$data->nama_dprd_id;?></td>
</tr>
   <td>&nbsp;</td>

<tr>
  <td></td>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td><?=$data->jenis_kelamin;?></td>

</tr>
   <td>&nbsp;</td>
<tr>
  <td></td>
    <td>Nomor Telepon</td>
    <td>:</td>
    <td><?=$data->notelp;?></td>
</tr>
   <td>&nbsp;</td>
<tr>
    <td></td>
    <td>Dapil</td>
    <td>:</td>
    <td><?=$data->nama_dapil;?></td>
</tr>
   <td>&nbsp;</td>
<tr>
    <td></td>
    <td>Fraksi</td>
    <td>:</td>
    <td><?=$data->nama_fraksi;?></td>
</tr>


</table>




                            </div>
                        </div>


                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
            
    </div>

<?php } ?>

      </div>
      <br>
     

          </div>
          <div class="card-footer small text-muted"></div>
        </div>



    </div>
  <!--   <div id="content-wrapper">
      <div class="container-fluid">
        <img src="<?=base_url()?>assets/b.jpg" width="1300px" height="550px">
      
      </div>
    </div> -->

    <!-- Area Chart Example-->
    <!-- <div class="card mb-3">
      <div class="card-header">
      <i class="fas fa-chart-area"></i>
      Visitor Stats</div>
      <div class="card-body">
      <canvas id="myAreaChart" width="100%" height="30"></canvas>
      </div>
      <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div> -->

    </div>
    <!-- /.container-fluid -->

    <!-- Sticky Footer -->
    <?php $this->load->view("admin/_partials/footer.php") ?>

  </div>
<!-- /#wrapper -->

<?php $this->load->view("dprd/_partials/scrolltop.php") ?>
<?php $this->load->view("dprd/_partials/modal.php") ?>
<?php $this->load->view("dprd/_partials/js.php") ?>

<script>
    function deleteConfirm(url){
      console.log(url);
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
    } 
  </script>

    
</body>
</html>