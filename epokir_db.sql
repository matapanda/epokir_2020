-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2019 at 12:14 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epokir_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_dprd`
--

CREATE TABLE `anggota_dprd` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_fraksi` int(11) NOT NULL,
  `id_dapil` int(11) NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `notelp` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_dprd`
--

INSERT INTO `anggota_dprd` (`id`, `nama`, `id_fraksi`, `id_dapil`, `jenis_kelamin`, `notelp`, `foto`) VALUES
(1, 'IWAN MAHENDRA, S.Sos., M.AP.', 1, 3, 'Laki-laki', '081333123727', '311.png'),
(2, 'EKO HERDIYANTO', 1, 1, 'Laki-laki', '083848560575', '29.png'),
(3, 'HARVAD KURNIAWAN R.,SH.', 1, 1, 'Laki-laki', '082245458867', '30.png'),
(4, 'Dra. WIWIK SUKESI D.R., M.Si', 1, 1, 'Perempuan', '081937717173', '28.png'),
(5, 'Drs. AGOES MARHAENTA, MH', 1, 2, 'Laki-laki', '08170505086', '25.png'),
(7, 'AMITHYA RATNANGGANI SIRRADUHITA, S.S', 1, 2, 'Perempuan', '08128881438', '26.png'),
(8, 'H. WANEDI', 1, 5, 'Laki-laki', '081273454909', '33.png'),
(9, 'LEA MAHDARINA, ST', 1, 5, 'Perempuan', '08113661993', '34.png'),
(10, 'NURUL SETYOWATI, SE', 1, 4, 'Perempuan', '085236323588', '24.png'),
(11, 'I MADE RIAN DIANA KARTIKA, SE', 1, 4, 'Laki-laki', '082244166630', '35.png'),
(12, 'FERRY KURNIAWAN', 1, 5, 'Laki-laki', '083835944269', '32.png'),
(13, 'LULUK ZUHRIYAH', 1, 2, 'Perempuan', '081333282550', '27.png'),
(14, 'ARIEF WAHYUDI, SH.', 7, 3, 'Laki-laki', '081291449009', '23.png'),
(15, 'ABDURROCHMAN, SH', 7, 1, 'Laki-laki', '08123594376', '39.png'),
(16, 'HARTATIK, SE.', 7, 1, 'Perempuan', '08123209570', '211.png'),
(17, 'ABD. WAHID', 7, 2, 'Laki-laki', '081217280344', '22.png'),
(18, 'IKE KISNAWATI, SH', 7, 2, 'Perempuan', '082142303134', '20.png'),
(19, 'Drs. H. FATHOL ARIFIN, M.H.', 7, 3, 'Laki-laki', '081945748750', '191.png'),
(20, 'AHMAD FARIH SULAIMAN, S.Pd.', 7, 4, 'Laki-laki', '085645447573', '181.png'),
(21, 'RIMZAH, S.IP', 2, 3, 'Laki-laki', '08113131189', '6.png'),
(22, 'Kol (Purn.) Drs. DJOKO HIRTONO, SSTF, M.Si', 2, 1, 'Laki-laki', '081353606049', '4.png'),
(23, 'NURUL FARIDAWATI', 2, 2, 'Perempuan', '087895807988', '21.png'),
(24, 'RANDY GAUNG KUMARANING AL ISLAM', 2, 5, 'Laki-laki', '081233695694', '31.png'),
(25, 'LELLY THRESIYAWATI', 2, 4, 'Perempuan', '08558553188', '5.png'),
(26, 'MOH ARIF BUDIARSO, ST', 3, 3, 'Laki-laki', '08125221764', '7.png'),
(27, 'EDDY WIDJANARKO, S.AP', 3, 1, 'Laki-laki', '0813332287', '141.png'),
(28, 'SURYADI,S.Pd.', 3, 2, 'Laki-laki', '081217359986', '121.png'),
(29, 'Drs. H. RAHMAN NURMALA, MM', 3, 5, 'Laki-laki', '082132345404', '131.png'),
(30, 'Hj. RETNO SUMARAH, SE, MM', 3, 4, 'Perempuan', '081252676677', '151.png'),
(31, 'Drs. SUYADI, MM', 8, 5, 'Laki-laki', '081222609992', '36.png'),
(32, 'GAGAH SOERYO PAMOEKTI', 8, 3, 'Laki-laki', '081945166904', '44.png'),
(33, 'H. BAYU REKSO AJI', 5, 3, 'Laki-laki', '081555752491', '10.png'),
(34, 'H. ASMUALIK', 5, 1, 'Laki-laki', '08135731316', '9.png'),
(35, 'H. AKHIDAYAT SYABRIL ULUM, S.Kom, MM', 5, 2, 'Laki-laki', '08563112235', '117.png'),
(36, 'H. ROKHMAD S.Sos', 5, 3, 'Laki-laki', '081216705222', '171.png'),
(37, 'AHMAD FUAD RAHMAN, SE', 5, 4, 'Laki-laki', '081223283456', '43.png'),
(38, 'TRIO AGUS PURWANTO, STP', 5, 4, 'Laki-laki', '08121728763', '118.png'),
(39, 'H. EKO HADI PURNOMO, SH', 4, 1, 'Laki-laki', '0816556764', '40.png'),
(40, 'H. LOOKH MAKHFUDZ, S.S', 4, 2, 'Laki-laki', '081357384921', '45.png'),
(41, 'H. PUJIANTO, SE., M.Hum', 4, 4, 'Laki-laki', '081334662013', '81.png'),
(42, 'INDAH NURDIANA, S.TP', 9, 1, 'Perempuan', '081252556369', '42.png'),
(43, 'H. IMRON', 9, 2, 'Laki-laki', '082231738303', '37.png'),
(44, 'WIWIK SULAIHA', 9, 5, 'Perempuan', '087865321244', '41.png'),
(45, 'DR. JOSE RIZAL JOESOEF, SE., M.Si.', 6, 4, 'Laki-laki', '081213141966', '161.png'),
(46, 'ALKASA SULIMA PRIYANTONO, SE', 10, 5, 'Laki-laki', '085234288799', '38.png');

-- --------------------------------------------------------

--
-- Table structure for table `aspirasi`
--

CREATE TABLE `aspirasi` (
  `id` int(11) NOT NULL,
  `usulan` varchar(255) NOT NULL,
  `volume` varchar(255) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `id_perangkat_daerah` int(11) NOT NULL,
  `id_anggota_dprd` int(11) NOT NULL,
  `id_dapil` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `foto_as` text NOT NULL,
  `status` enum('Belum Dikonfirmasi','Diterima','Ditolak') NOT NULL DEFAULT 'Belum Dikonfirmasi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aspirasi`
--

INSERT INTO `aspirasi` (`id`, `usulan`, `volume`, `id_satuan`, `lokasi`, `tanggal`, `id_perangkat_daerah`, `id_anggota_dprd`, `id_dapil`, `keterangan`, `foto_as`, `status`) VALUES
(8, 'banjir', '12', 5, 'sawojajar', '2019-12-13', 18, 1, 3, '', '6c158dc586e4980cbb601e1bcb824b8db8255098c4387967cfe69c80a6bbfa49.jpg', 'Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `dapil`
--

CREATE TABLE `dapil` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dapil`
--

INSERT INTO `dapil` (`id`, `nama`, `kode`) VALUES
(1, 'Blimbing', '65126'),
(2, 'Kedungkandang', '65139'),
(3, 'Klojen', '65111'),
(4, 'Lowokwaru', '65412'),
(5, 'Sukun', '65476');

-- --------------------------------------------------------

--
-- Table structure for table `fraksi`
--

CREATE TABLE `fraksi` (
  `id` int(11) NOT NULL,
  `nama_fraksi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fraksi`
--

INSERT INTO `fraksi` (`id`, `nama_fraksi`) VALUES
(1, 'PDIP'),
(2, 'GERINDRA'),
(3, 'GOLKAR'),
(4, 'PAN'),
(5, 'PKS'),
(6, 'PSI'),
(7, 'PKB'),
(8, 'NASDEM'),
(9, 'DEMOKRAT'),
(10, 'PERINDO');

-- --------------------------------------------------------

--
-- Table structure for table `perangkat_daerah`
--

CREATE TABLE `perangkat_daerah` (
  `id` int(11) NOT NULL,
  `nama_perangkat` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perangkat_daerah`
--

INSERT INTO `perangkat_daerah` (`id`, `nama_perangkat`, `alamat`, `email`) VALUES
(1, 'Sekretariat Daerah', '', ''),
(2, 'Sekretariat DPRD', '', ''),
(3, 'Inspektorat', '', ''),
(4, 'Badan Kepegawaian dan Pengembangan Sumber Daya Manusia', '', ''),
(5, 'Badan Kesatuan Bangsa dan Politik', '', ''),
(6, 'Badan Pendapatan Daerah', '', ''),
(7, 'Badan Penanggulangan Bencana Daerah', '', ''),
(8, 'Satuan Polisi Pamong Praja', '', ''),
(9, 'Dinas Tenaga Kerja dan Penanaman Modal dan  Pelayanan Terpadu Satu Pintu (PMPTSP)', '', ''),
(10, 'Dinas Koperasi, Perindustrian dan Perdagangan', '', ''),
(11, 'Dinas Komunikasi dan Informatika', '', ''),
(12, 'Dinas Kepemudaan, Olahraga dan Pariwisata', '', ''),
(13, 'Badan Perencanaan Pembangunan Daerah', '', ''),
(14, 'Dinas Perpustakaan Umum dan Arsip Daerah', '', ''),
(15, 'Dinas Pendidikan dan Kebudayaan', '', ''),
(16, 'Dinas Kesehatan', '', ''),
(17, 'Dinas Perhubungan', '', ''),
(18, 'Dinas Ketahanan Pangan dan Pertanian', '', ''),
(19, 'Dinas Pekerjaan Umum, Penataan Ruang,Perumahan dan Kawasan Permukiman', '', ''),
(21, 'Dinas Sosial, Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana', '', ''),
(24, 'Dinas Kependudukan dan Pencatatan Sipil', '', ''),
(25, 'Dinas Lingkungan Hidup', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`) VALUES
(1, 'Unit'),
(2, 'Buah'),
(3, 'Lembar'),
(4, 'Meter'),
(5, 'Hektar'),
(6, 'm2'),
(7, 'Luas');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `level` enum('admin','dprd') DEFAULT NULL,
  `id_dprd` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `foto`, `level`, `id_dprd`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin', NULL),
(2, 'NURUL FARIDAWATI', 'nurulfaridawati', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 23),
(3, 'H. AKHIDAYAT SYABRIL ULUM, S.Kom, MM', 'akhidat', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 35),
(4, 'RANDY GAUNG KUMARANING AL ISLAM', 'randy', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 24),
(14, 'Kol (Purn.) Drs. DJOKO HIRTONO, SSTF, M.Si', 'djoko', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 22),
(15, 'LELLY THRESIYAWATI', 'lelly', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 25),
(17, 'RIMZAH, S.IP', 'rimzah', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 21),
(18, 'MOH ARIF BUDIARSO, ST', 'arifbudiarso', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 26),
(19, 'H. PUJIANTO, SE., M.Hum', 'pujianto', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 41),
(20, 'H. ASMUALIK', 'asmualik', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 34),
(21, 'H. BAYU REKSO AJI', 'bayu', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 33),
(22, 'TRIO AGUS PURWANTO, STP', 'trio', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 38),
(23, 'SURYADI,S.Pd.', 'suryadi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 28),
(24, 'Drs. H. RAHMAN NURMALA, MM', 'rahman', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 29),
(25, 'EDDY WIDJANARKO, S.AP', 'eddy', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 27),
(26, 'Hj. RETNO SUMARAH, SE, MM', 'retno', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 30),
(27, 'DR. JOSE RIZAL JOESOEF, SE., M.Si.', 'jose', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 45),
(28, 'H. ROKHMAD S.Sos', 'rokhmad', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 36),
(29, 'AHMAD FARIH SULAIMAN, S.Pd.', 'ahmadfarih', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 20),
(30, 'Drs. H. FATHOL ARIFIN, M.H.', 'fathol', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 19),
(31, 'IKE KISNAWATI, SH', 'ike', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 18),
(32, 'HARTATIK, SE.', 'hartatik', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 16),
(33, 'ABD. WAHID', 'wahid', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 17),
(34, 'ARIEF WAHYUDI, SH.', 'ariefwahyudi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 14),
(35, 'NURUL SETYOWATI, SE', 'nurulsetyowati', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 10),
(36, 'Drs. AGOES MARHAENTA, MH', 'agoes', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 5),
(37, 'AMITHYA RATNANGGANI SIRRADUHITA, S.S', 'amithya', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 7),
(38, 'LULUK ZUHRIYAH', 'luluk', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 13),
(39, 'Dra. WIWIK SUKESI D.R., M.Si', 'wiwiksukesi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 4),
(40, 'EKO HERDIYANTO', 'eko', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 2),
(41, 'HARVAD KURNIAWAN R.,SH.', 'harvad', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 3),
(42, 'IWAN MAHENDRA, S.Sos., M.AP.', 'iwan', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 1),
(43, 'FERRY KURNIAWAN', 'ferry', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 12),
(44, 'H. WANEDI', 'wanedi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 8),
(45, 'LEA MAHDARINA, ST', 'lea', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 9),
(46, 'I MADE RIAN DIANA KARTIKA, SE', 'made', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 11),
(47, 'Drs. SUYADI, MM', 'suyadi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 31),
(48, 'H. IMRON', 'imron', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 43),
(49, 'ALKASA SULIMA PRIYANTONO, SE', 'alkasa', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 46),
(50, 'ABDURROCHMAN, SH', 'abdurrochman', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 15),
(51, 'H. EKO HADI PURNOMO, SH', 'ekohadi', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 39),
(52, 'WIWIK SULAIHA', 'wiwiksulaiha', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 44),
(53, 'INDAH NURDIANA, S.TP', 'indah', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 42),
(54, 'AHMAD FUAD RAHMAN, SE', 'ahmadfuad', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 37),
(55, 'GAGAH SOERYO PAMOEKTI', 'gagah', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 32),
(56, 'H. LOOKH MAKHFUDZ, S.S', 'lookh', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'dprd', 40);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_dprd`
--
ALTER TABLE `anggota_dprd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dapil` (`id_dapil`),
  ADD KEY `id_fraksi` (`id_fraksi`);

--
-- Indexes for table `aspirasi`
--
ALTER TABLE `aspirasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dapil` (`id_dapil`),
  ADD KEY `id_anggota_dprd` (`id_anggota_dprd`),
  ADD KEY `id_perangkat_daerah` (`id_perangkat_daerah`),
  ADD KEY `id_satuan` (`id_satuan`);

--
-- Indexes for table `dapil`
--
ALTER TABLE `dapil`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `fraksi`
--
ALTER TABLE `fraksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perangkat_daerah`
--
ALTER TABLE `perangkat_daerah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_dprd` (`id_dprd`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_dprd`
--
ALTER TABLE `anggota_dprd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `aspirasi`
--
ALTER TABLE `aspirasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `dapil`
--
ALTER TABLE `dapil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fraksi`
--
ALTER TABLE `fraksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `perangkat_daerah`
--
ALTER TABLE `perangkat_daerah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggota_dprd`
--
ALTER TABLE `anggota_dprd`
  ADD CONSTRAINT `anggota_dprd_ibfk_2` FOREIGN KEY (`id_dapil`) REFERENCES `dapil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `anggota_dprd_ibfk_3` FOREIGN KEY (`id_fraksi`) REFERENCES `fraksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aspirasi`
--
ALTER TABLE `aspirasi`
  ADD CONSTRAINT `aspirasi_ibfk_1` FOREIGN KEY (`id_dapil`) REFERENCES `dapil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aspirasi_ibfk_2` FOREIGN KEY (`id_anggota_dprd`) REFERENCES `anggota_dprd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aspirasi_ibfk_3` FOREIGN KEY (`id_perangkat_daerah`) REFERENCES `perangkat_daerah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aspirasi_ibfk_4` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_dprd`) REFERENCES `anggota_dprd` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
