<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class aspirasi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Aspirasi_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["aspirasi"] = $this->Aspirasi_model->getAll();
		$this->load->view("admin/aspirasi/list", $data);
	}

	public function tampil() {
		$data['aspirasi'] = $this->Aspirasi_model->select_all();
		$this->load->view('admin/aspirasi/list', $data);
	}

	public function add()
	{
		$aspirasi = $this->Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		if ($validation->run())
		{
			$aspirasi->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
		}

		$this->load->view("admin/aspirasi/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/aspirasi');

		$aspirasi = $this->Aspirasi_model;
		$validation = $this->form_validation;
		$validation->set_rules($aspirasi->rules());

		if ($validation->run())
		{
			$aspirasi->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
		}

		$data["aspirasi"] = $aspirasi->getById($id);
		if (!$data["aspirasi"]) show_404();

		$this->load->view("admin/aspirasi/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Aspirasi_model->delete($id))
		{
			redirect(site_url('admin/aspirasi'));
		}
	}

}