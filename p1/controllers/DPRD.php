<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dprd extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dprd_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$data["dprd"] = $this->Dprd_model->getAll();
		$this->load->view("admin/dprd/list", $data);
	}

	public function tampil() {
		$data['dprd'] = $this->Dprd_model->select_all();
		$this->load->view('admin/dprd/list', $data);
	}

	public function add()
	{
		$dprd = $this->Dprd_model;
		$validation = $this->form_validation;
		$validation->set_rules($dprd->rules());

		if ($validation->run())
		{
			$dprd->save();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
		}

		$this->load->view("admin/dprd/new_form");
	}

	public function edit($id = null)
	{
		if (!isset($id)) redirect ('admin/dprd');

		$dprd = $this->Dprd_model;
		$validation = $this->form_validation;
		$validation->set_rules($dprd->rules());

		if ($validation->run())
		{
			$dprd->update();
			$this->session->set_flashdata('success', 'Berhasil Disimpan');
		}

		$data["dprd"] = $dprd->getById($id);
		if (!$data["dprd"]) show_404();

		$this->load->view("admin/dprd/edit_form", $data); 
	}

	public function delete($id = null)
	{
		if (!isset($id)) show_404();

		if ($this->Dprd_model->delete($id))
		{
			redirect(site_url('admin/dprd'));
		}
	}

	public function create() 
    {
        // load model dan form helper 
        $this->load->model('Dprd_model');
        $this->load->helper('form_helper');
 
        $data = array(
            'button' => 'Create',
            'action' => site_url('dprd/create_action'),
            'dd_kelamin' => $this->Dprd_model->dd_kelamin(),
            'kelamin_selected' => $this->input->post('dprd') ? $this->input->post('dprd') : $row->kelamin, // untuk edit ganti '' menjadi data dari database misalnya $row->provinsi
	);
        
        $this->load->view('provinsi_form', $data);
    }

}