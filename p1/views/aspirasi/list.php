<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/aspirasi/add') ?>"><i class="fas fa-plus"></i> Add New</a>
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" >
								<thead>
									<tr>
										<th>ID</th>
										<th>Anggota DPRD</th>
										<th>Dapil</th>
										<th>Tanggal</th>
										<th>Usulan</th>
										<th>Lokasi</th>
										<th>Biaya</th>
										<th>Status</th>
										<th style="text-align: center;">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($aspirasi as $aspirasi): ?>
									<tr>
										<td><?php echo $aspirasi->id ?></td>
										<td style="min-width:230px;"><?php echo $aspirasi->id_anggota_dprd ?></td>
										<td><?php echo $aspirasi->id_dapil ?></td>
										<td><?php echo $aspirasi->tanggal ?></td>
										<td><?php echo $aspirasi->usulan ?></td>
										<td><?php echo $aspirasi->lokasi ?></td>
										<td><?php echo $aspirasi->biaya ?></td>
										<td><?php echo $aspirasi->status ?></td>
										
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

</body>

</html>