<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">


	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/aspirasi/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php base_url('admin/aspirasi/add') ?>" method="post" enctype="multipart/form-data" >
							
							<!-- <div class="form-group">
								<label for="name">ID </label>
								<input class="form-control <?php echo form_error('id') ? 'is-invalid':'' ?>"
								 type="number" name="id"   />
								<div class="invalid-feedback">
									<?php echo form_error('id') ?>
								</div>
							</div> -->

							<div class="form-group">
								<label for="id_anggota_dprd">Anggota DPRD*</label>
								<input class="form-control <?php echo form_error('id_anggota_dprd') ? 'is-invalid':'' ?>"
								 type="text" name="id_anggota_dprd" placeholder="Nama Anggota DPRD" />
								<div class="invalid-feedback">
									<?php echo form_error('id_anggota_dprd') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="iddapil">ID Dapil*</label>
								<input class="form-control <?php echo form_error('id_dapil') ? 'is-invalid':'' ?>"
								 type="text" name="id_dapil"  placeholder="ID Dapil" />
								<div class="invalid-feedback">
									<?php echo form_error('id_dapil') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tanggal">Tanggal*</label>
								<input class="form-control <?php echo form_error('tanggal') ? 'is-invalid':'' ?>"
								 type="date" name="tanggal"  placeholder="Tanggal" />
								<div class="invalid-feedback">
									<?php echo form_error('tanggal') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="usulan">Usulan*</label>
								<textarea class="form-control <?php echo form_error('usulan') ? 'is-invalid':'' ?>"
								 name="usulan" placeholder="Inputkan usulan"></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('usulan') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="lokasi">Alamat*</label>
								<input textarea class="form-control <?php echo form_error('lokasi') ? 'is-invalid':'' ?>"
								 type="text" name="lokasi"  placeholder="Inputkan alamat tempat yang diusulkan" />
								<div class="invalid-feedback">
									<?php echo form_error('lokasi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="biaya">Perkiraan Biaya*</label>
								<input class="form-control <?php echo form_error('biaya') ? 'is-invalid':'' ?>"
								 type="number" name="biaya"  placeholder="Inputkan perkiraan biaya" />
								<div class="invalid-feedback">
									<?php echo form_error('biaya') ?>
								</div>
							</div>




							 


							 <!-- <div class="form-group">
								<label for="name">Photo</label>
								<input class="form-control-file <?php echo form_error('price') ? 'is-invalid':'' ?>"
								 type="file" name="image" />
								<div class="invalid-feedback">
									<?php echo form_error('image') ?>
								</div>
							</div> -->
						<!--
							<div class="form-group">
								<label for="name">Description*</label>
								<textarea class="form-control <?php echo form_error('description') ? 'is-invalid':'' ?>"
								 name="description" placeholder="Product description..."></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('description') ?>
							</div>
						</div> -->

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>