<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/dprd/add') ?>"><i class="fas fa-plus"></i> Add New</a>
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="dataTable" >
								<thead>
									<tr>
										<th>ID</th>
										<th>Nama</th>
										<th>ID DAPIL</th>
										<th>Jenis Kelamin</th>
										<th>No. Telepon</th>
										<th>Foto</th>
										<th style="text-align: center;">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($dprd as $dprd): ?>
									<tr>
										<td><?php echo $dprd->id ?></td>
										<td style="min-width:230px;"><?php echo $dprd->nama ?></td>
										<td><?php echo $dprd->id_dapil ?></td>
										<td><?php echo $dprd->id_kelamin ?></td>
										<td><?php echo $dprd->notelp ?></td>
										<td>
											<img src="<?php echo base_url('upload/product/'.$dprd->foto) ?>" width="64" />
										</td>
										<td width="250">
											<a href="<?php echo site_url('admin/dprd/edit/'.$dprd->id) ?>"
											 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/dprd/delete/'.$dprd->id) ?>')"
											 href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

	<script>
		function deleteConfirm(url){
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}	
	</script>

	

</body>

</html>