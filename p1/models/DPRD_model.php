<?php
class Dprd_model extends CI_Model {
 
    private $_table = "anggota_dprd";
    // private $_table = "Kelamin";

    public $id;
    public $nama;
    public $id_dapil;
    public $id_kelamin;
    public $notelp;
    public $foto;


    public function rules()
    {
        return [

            ['field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'],
            
            ['field' => 'id_kelamin',
            'label' => 'Kelamin',
            'rules' => 'required'],

            ['field' => 'notelp',
            'label' => 'No Telepon',
            'rules' => 'numeric']
        ];
    }

    // get data dropdown
    function dd_kelamin()
    {
        // ambil data dari db
        $this->db->order_by('kelamin', 'asc');
        $result = $this->db->get('kelamin');
        
        // bikin array
        // please select berikut ini merupakan tambahan saja agar saat pertama
        // diload akan ditampilkan text please select.
        $dd[''] = 'Please Select';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
            // tentukan value (sebelah kiri) dan labelnya (sebelah kanan)
                $dd[$row->id] = $row->kelamin;
            }
        }
        return $dd;
    }

    public function getAll()
    {
    	return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	//$this->id = uniqid();
    	$this->nama = $post["nama"];
        $this->id_dapil = $post["id_dapil"];
        $this->id_kelamin = $post["id_kelamin"];
        $this->notelp = $post["notelp"];
        $this->foto = $this->_uploadImage();
    	$this->db->insert($this->_table, $this);

    }

    

    public function update()
    {
    	$post = $this->input->post();
    	$this->id = $post["id"];
    	$this->nama = $post["nama"];
    	$this->id_dapil = $post["id_dapil"];
        $this->id_kelamin = $post["id_kelamin"];
        $this->notelp = $post["notelp"];
        
        if (!empty($_FILES["foto"]["nama"])) {
            $this->foto = $this->_uploadImage();
        } else {
            $this->foto = $post["old_image"];
        }

    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }


    public function delete($id)
    {
        $this->_deleteImage($id);
    	return $this->db->delete($this->_table, array('id' => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $dprd = $this->getById($id);
        if ($dprd->foto != "default.jpg") {
            $filename = explode(".", $dprd->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
    }
}
}