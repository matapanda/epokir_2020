<?php
class Aspirasi_model extends CI_Model {
 
    private $_table = "aspirasi";

    public $id;
    public $id_anggota_dprd;
    public $id_dapil;
    public $tanggal;
    public $usulan;
    public $lokasi;
    public $biaya;
    public $status;


    public function rules()
    {
        return [

            ['field' => 'tanggal',
            'label' => 'tanggal',
            'rules' => 'required'],
            
            ['field' => 'usulan',
            'label' => 'usulan',
            'rules' => 'required'],

            ['field' => 'lokasi',
            'label' => 'lokasi',
            'rules' => 'required'],

            ['field' => 'biaya',
            'label' => 'biaya',
            'rules' => 'required']

        ];
    }

    public function getAll()
    {
    	return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
    	return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
    	$post = $this->input->post();
    	//$this->id = uniqid();
    	$this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->tanggal = $post["tanggal"];
        $this->usulan = $post["usulan"];
        $this->lokasi = $post["lokasi"];
        $this->biaya = $post["biaya"];
    	$this->db->insert($this->_table, $this);

    }

    

    public function update()
    {
    	$post = $this->input->post();
    	$this->id_anggota_dprd = $post["id_anggota_dprd"];
        $this->id_dapil = $post["id_dapil"];
        $this->tanggal = $post["tanggal"];
        $this->usulan = $post["usulan"];
        $this->lokasi = $post["lokasi"];
        $this->biaya = $post["biaya"];

    	$this->db->update($this->_table, $this, array('id' => $post['id']));
    }


    public function delete($id)
    {
        $this->_deleteImage($id);
    	return $this->db->delete($this->_table, array('id' => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/product/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $dprd = $this->getById($id);
        if ($dprd->foto != "default.jpg") {
            $filename = explode(".", $dprd->foto)[0];
            return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
    }
}
}